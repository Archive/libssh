#include <glib.h>
#include <glib-object.h>

#include <unistd.h>

#include <libssh/ssh-transport.h>
#include <libssh/ssh-userauth.h>
#include <libssh/ssh-filexfer.h>

typedef struct _ShellState {
	GMainLoop		*mainloop;
	GIOChannel		*input;
	GSource			*source;
	int			 source_callback_id;
	SshTransport 		*transport;
	SshUserauth		*userauth;
	SshFilexfer 	*filexfer;
} ShellState;

/* test:testpass */

static void
auth_success (SshUserauth *u, gboolean successful, gpointer user_data)
{
	ShellState *state = user_data;

	g_print ("[YAK] auth_success\n");

	state->filexfer = ssh_filexfer_new (state->transport);

	g_print ("[YAK] filexfer_new complete.\n");
}

static void
auth_banner (SshUserauth *u, const gchar *message, const gchar *language,
		gpointer user_data)
{
	g_print ("[YAK] auth_banner in language: %s\nbanner: %s\n", 
			language, message);
}

static gchar *
auth_password (SshUserauth *u, gpointer user_data)
{
	g_print ("[YAK] auth_password\n");
	return g_strdup ("testpass");
}

static void
transport_connected (SshTransport *t, gpointer user_data)
{
	ShellState *state = user_data;

	state->userauth = ssh_userauth_new (t, "test");

	g_signal_connect (state->userauth, "success", (GCallback)auth_success,
			state);
	g_signal_connect (state->userauth, "banner", (GCallback)auth_banner,
			state);
	g_signal_connect (state->userauth, "password", (GCallback)auth_password,
			state);

	ssh_userauth_start (state->userauth);
}

int
main (int argc, char **argv) 
{

	ShellState state;

	int port = 24;
	char *host = "localhost";

	g_type_init ();

	if (argc > 1) {
		port = atoi (argv[1]); /* hacks */
	}

	state.mainloop = g_main_loop_new (NULL, FALSE);

	state.transport = ssh_transport_new ();

	g_signal_connect (G_OBJECT (state.transport), "connected", 
			(GCallback)transport_connected, &state);

	ssh_transport_connect (state.transport, host, port);

	g_main_loop_run (state.mainloop);
#if 0
	for (;;) {
		ssh_transport_handle_packet (state.transport);
	}
#endif
	return 0;
}
