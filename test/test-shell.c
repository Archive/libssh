#include <glib.h>
#include <glib-object.h>

#include <unistd.h>

#include <libssh/ssh-transport.h>
#include <libssh/ssh-userauth.h>
#include <libssh/ssh-channel-session.h>

typedef struct _ShellState {
	GMainLoop		*mainloop;
	GIOChannel		*input;
	GSource			*source;
	int			 source_callback_id;
	SshTransport 		*transport;
	SshUserauth		*userauth;
	SshChannelSession 	*session;
} ShellState;

/* test:testpass */

static void
data_callback (SshChannel *channel, SshBuffer *buffer)
{
	write (1, ssh_buffer_bytes (buffer), ssh_buffer_length (buffer));
}

static void
extended_data_callback (SshChannel *channel, guint32 type, SshBuffer *buffer)
{
	write (2, ssh_buffer_bytes (buffer), ssh_buffer_length (buffer));
}

static void
session_channel_success (SshChannel *channel, gpointer user_data) {
	ShellState *state = user_data;

	g_signal_connect (G_OBJECT (state->session), "data",
			(GCallback)data_callback, state);
	g_signal_connect (G_OBJECT (state->session), "extended_data",
			(GCallback)extended_data_callback, state);

	ssh_channel_session_pty_req (state->session, g_getenv("TERM"),
			80, 25, 640, 480, "");
	ssh_channel_session_shell (state->session);
}

static void
auth_success (SshUserauth *u, gboolean successful, gpointer user_data)
{
	ShellState *state = user_data;

	state->session = ssh_channel_session_new (state->transport);

	g_signal_connect (G_OBJECT (state->session), "success",
			(GCallback) session_channel_success, state);
}

static void
auth_banner (SshUserauth *u, const gchar *message, const gchar *language,
		gpointer user_data)
{
	g_print ("[YAK] auth_banner in language: %s\nbanner: %s\n", 
			language, message);
}

static gchar *
auth_password (SshUserauth *u, gpointer user_data)
{
	g_print ("[YAK] auth_password\n");
	return g_strdup ("testpass");
}

static void
transport_connected (SshTransport *t, gpointer user_data)
{
	ShellState *state = user_data;

	state->userauth = ssh_userauth_new (t, "test");

	g_signal_connect (state->userauth, "success", (GCallback)auth_success,
			state);
	g_signal_connect (state->userauth, "banner", (GCallback)auth_banner,
			state);
	g_signal_connect (state->userauth, "password", (GCallback)auth_password,
			state);

	ssh_userauth_start (state->userauth);
}

static gboolean
input_ready (GIOChannel *source, GIOCondition condition, gpointer user_data)
{
	ShellState *state = user_data;
	gchar buf[1024];
	gint len = 0;
	GIOStatus status;
	GError *error = NULL;

	status = g_io_channel_read_chars (source, buf, 1023, &len, &error);

	if (status == G_IO_STATUS_NORMAL) {
		if (len > 0) {
			buf[len] = 0;
			ssh_channel_write_cstring (SSH_CHANNEL (state->session),
					buf);
		}
	} else {
		return FALSE;
	}

	return TRUE;
}



int
main (int argc, char **argv) 
{

	ShellState state;

	int port = 24;
	char *host = "localhost";
	GError *error = NULL;

	g_type_init ();

	state.mainloop = g_main_loop_new (NULL, FALSE);

	state.input = g_io_channel_unix_new (0);
	g_io_channel_set_flags (state.input, G_IO_FLAG_NONBLOCK, &error);
	g_io_channel_set_encoding (state.input, NULL, &error);
	g_io_channel_set_buffered (state.input, FALSE);
	if (error != NULL) {
		g_print ("[YAK] g_io_channel_set_flags returned an error\n");
	}
	state.source = g_io_create_watch (state.input, G_IO_IN);
	g_source_set_callback (state.source, (GSourceFunc)input_ready,
			&state, NULL);
	state.source_callback_id = g_source_attach (state.source, NULL /*ctx*/);

	state.transport = ssh_transport_new ();

	g_signal_connect (G_OBJECT (state.transport), "connected", 
			(GCallback)transport_connected, &state);

	ssh_transport_connect (state.transport, host, port);

	g_main_loop_run (state.mainloop);
#if 0
	for (;;) {
		ssh_transport_handle_packet (state.transport);
	}
#endif
	return 0;
}
