 /* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#ifndef __SSH_TRANSPORT_H__
#define __SSH_TRANSPORT_H__

#include <openssl/dh.h>
#include <openssl/blowfish.h>
//#include <openssl/evp.h>

#include <string>
#include <vector>
#include <map>

#include "ssh-authenticator.h"
#include "ssh-diffiehellman.h"
#include "ssh-engine.h"
#include "ssh-packethandler.h"
#include "ssh-stream.h"

using namespace std;

namespace SSH {
	class Channel;
	class Transport;
	class Authenticator;
	class Userauth;
	class HMac;
	class Buffer;

	class TransportListener {
		public:
			virtual void transportConnected (Transport *t) {};
			virtual void transportAuthenticated (Transport *t) {};
	};

	class TransportException {
		public:
			TransportException (const string& s) : msg(s) {
				printf ("TransportException: %s\n", s.c_str());
			}
			string message () { return msg; }
		private:
			string msg;
	};

	class Transport {
		friend class Channel;
		friend class Userauth;
		public:
				Transport 	(Stream *s,
						 Authenticator *a);
			void	handlePacket 	();

			void	addPacketHandler (PacketHandler *ph);
			void	removePacketHandler (PacketHandler *ph);

			void	addTransportListener (TransportListener *tl);
			void	removeTransportListener (TransportListener *tl);

			void	sendPacket 		(const Buffer &p);
			void	sendSERVICE_REQUEST 	(const string& service);

			void	authenticate	(Authenticator *auth);
		protected:
			uint32_t addChannel 	(Channel *c);
			Channel *channel	(uint32_t id);
			Channel *channel	(const Buffer& b);

		private:
			void		bufferFill ();
			void		bufferFill (uint32_t length);
			uint32_t	readUint32 ();
			Buffer*		readBytes (uint32_t length);
			char		readByte ();
			char		peekByte ();
			char*		readLine ();
			void		write (const char *bytes, uint32_t len);
			void		write (const Buffer &p);
			void		write (Buffer* p);
			void		write (const string& string);
			Buffer*		readBlock ();
			Buffer*		recvPacket ();
			Buffer*		calculateMacOut (const Buffer &p);
			void		sendKEXINIT ();
			void		sendKEXDH_INIT ();
			void		sendNEWKEYS ();
			void		recvKEXDH_REPLY (const Buffer &p);
			Buffer*		computeKey (char id, uint32_t length);
			void		recvNEWKEYS (const Buffer &p);
			void		recvKEXINIT (const Buffer &p);
			void		recvSERVICE_ACCEPT (const Buffer &p);
#if 0
		void (* connected) (SshTransport *transport);
		bool (* packet) (SshTransport *t, Buffer *p);
#endif

			Stream 		*stream;

			void 		*user_data;

			uint32_t	 sequence_number_out;
			uint32_t	 sequence_number_in;

			string mac_algorithm_client_to_server;
			string mac_algorithm_server_to_client;
			HMac		*mac_in;
			HMac		*mac_out;

			int		 block_size;

			char 		*V_C;
			char 		*V_S;

			char 		*internal_buffer;
			int		 internal_buffer_len;
			char 		*internal_buffer_base_ptr;

			Buffer 		*I_C;
			Buffer 		*I_S;

			Bignum 		*K;
			Bignum 		*e;
			Bignum 		*f;

			Buffer 		*H;

			DiffieHellman	*dh;

			Buffer 		*IV_out;
			Buffer 		*IV_in;
			BF_KEY		 bf_key_out;
			BF_KEY		 bf_key_in;

			Engine 		*engine_out;
			Engine 		*engine_in;

			/* connection / channel stuff */
			int 		next_channel_id;
			map<int,Channel*>	channels;

			typedef vector<PacketHandler *> PacketHandlers;
			PacketHandlers		packetHandlers;

			typedef vector<TransportListener *> TransportListeners;
			TransportListeners	transportListeners;
			/* TransportListener events */
			void connected ();
			void authenticated ();

			/* Authentication stuff */
			Authenticator	*authenticator;
			Userauth	*userauth;
	};
};

#if 0
void		 auth_password 	(SshTransport	*t,
						 char		*username, 
						 char		*password);
#endif

#endif /* __SSH_TRANSPORT_H__ */
