/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#ifndef SSH_STREAM_H
#define SSH_STREAM_H

namespace SSH {

	/*
	 * This is an interface to a lower-level socket implementation.
	 */
	class Stream {
		public:
			virtual void read (char	*buffer,
					   int	 length,
					   int	*length_read) = 0;
			virtual void write (const char	*buffer,
					    int		 length,
					    int		*length_written) = 0;
			virtual int fd () = 0;
	};

} /* namespace SSH */


#endif /* SSH_STREAM_H */

