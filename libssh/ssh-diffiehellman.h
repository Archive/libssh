/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

/*
 * This class wraps bignum so we can swap out OpenSSL for something else at
 * a later date.
 */

#ifndef __SSH_DIFFIEHELLMAN_H__
#define __SSH_DIFFIEHELLMAN_H__

#include <string>
#include <inttypes.h>
#include <openssl/dh.h>

#include "ssh-buffer.h"

using namespace std;

namespace SSH {
	class DiffieHellman {
		public:
			DiffieHellman 	();
			~DiffieHellman	();

			Bignum *	publicKey () const;
			Bignum *	computeKey (Bignum *key) const;
		private:
			DH *dh;
			bool publicKeyValid () const;
	};

};

#endif /* __SSH_DIFFIEHELLMAN_H__ */
