/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

/* this provides interfaces for an application to override for fetching
 * authentication information from a user
 */

#ifndef SSH_AUTHENTICATOR_H
#define SSH_AUTHENTICATOR_H

#include <string>

using namespace std;

namespace SSH {

	class Authenticator {
		public:
			virtual string *getUsername ();
			virtual string *getPassword (const string& username);
			virtual void showBanner     (const string& message,
						     const string& language);
			virtual void success 	    (bool successful);


	};


};

#endif /* SSH_AUTHENTICATOR_H */
