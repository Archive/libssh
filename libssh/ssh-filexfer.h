/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

/* The native, async filexfer API */


#ifndef __SSH_FILEXFER_H__
#define __SSH_FILEXFER_H__

#include "ssh-channel.h"
#include "ssh-session-channel.h"
#include "ssh-buffer.h"
#include "ssh-transport.h"

#include <map>
#include <string>

using namespace std;

namespace SSH {
	class Filexfer :	public SessionChannel, 
				protected ChannelListener {
		class Attribute {
			/* This doesn't work when its const. extended gets
			 * all confused. We should fix this. FIXME */
			public:
				uint32_t		flags;
				uint64_t		size;
				uint32_t		uid;
				uint32_t		gid;
				uint32_t		permissions;
				uint32_t		atime;
				uint32_t		mtime;
				typedef map<string,string> Extended;
				Extended		extended;

				Attribute (Buffer *b);
				void append (Buffer *b);
		};

		class Info {
			public:
				Info (Buffer *b);
				~Info ();
				string			filename;
				string			longname;
				Filexfer::Attribute*	attrs;
		};

		class Listener {
			public:
				virtual void fxpStatus (uint32_t            id,
							uint32_t            code) = 0;
				virtual void fxpHandle (uint32_t            id,
							const Buffer*       handle) = 0;
				virtual void fxpData   (uint32_t            id,
							const Buffer*       data) = 0;
				virtual void fxpName   (uint32_t            id,
							vector<const Filexfer::Info*>   
									    names) = 0;
				virtual void fxpAttrs  (uint32_t            id,
							const Filexfer::Attribute *attr) = 0;
				/* FIXME: handle extended */

		};

		class Response {
			public:
				Response  (uint8_t t, Buffer *b);
				~Response ();
				uint8_t type;
				uint32_t id;
				/*union {*/
					uint32_t	status;
					Buffer *	handle;
					Buffer *	data;
					vector<Info*>	name;
					Attribute*	attrs;
					/* FIXME: support extended thingos */
				/*} u;*/
		};

		public:
			Filexfer		(Transport*	t);
			bool connected		();

			uint32_t open		(const string& filename,
		 				 uint32_t pflags,
						 Filexfer::Attribute& attrs);
			uint32_t close		(const Buffer& handle);
			uint32_t read		(const Buffer& handle, 
						 uint64_t offset,
						 uint32_t length);
			uint32_t write		(const Buffer& handle, 
						 uint64_t offset,
						 const Buffer& data);
			uint32_t remove		(const string& filename);
			uint32_t rename		(const string& oldpath, 
						 const string& newpath);
			uint32_t mkdir		(const string& path);
			uint32_t rmdir		(const string& path);
			uint32_t opendir	(const string& path);
			uint32_t readdir	(const Buffer& handle);
			uint32_t stat		(const string& path);
			uint32_t lstat		(const string& path);
			uint32_t fstat		(const Buffer& handle);
			uint32_t setstat	(const string& path, 
						 Filexfer::Attribute& attr);
			uint32_t fsetstat	(const Buffer& handle, 
						 Filexfer::Attribute& attr);
			uint32_t realpath	(const string& path);

			Response* getResponse	(uint32_t id);
			Response* waitForResponse (uint32_t id);


		protected:
			virtual void channelOpened 	(Channel  *c);
			virtual void channelData 	(Channel  *c,
							 Buffer   *b);
			virtual void channelExtendedData(Channel  *c, 
							 uint32_t  type, 
							 Buffer   *data) {
				data->debug ("FXP extended");
			};

		private:
			void sendPacket (uint8_t type, const Buffer& data);
			uint32_t sendRequest (uint8_t type, const Buffer& data);
			void sendINIT ();
			void handle (uint8_t t, Buffer *b);
			void handleResponse (uint8_t t, Buffer *b);

			Buffer buffer;

			bool have_header;
			uint32_t length;
			uint8_t type;

			uint32_t next_id;

			bool have_remote_version;
			uint32_t remote_version;

			typedef vector<Response *> Responses;
			vector<Response *> pending_responses;
	};
};
 


#endif /* __SSH_FILEXFER_H__ */

