/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#ifndef SSH_BUFFER_H
#define SSH_BUFFER_H

#include <openssl/bn.h>
#include <stdint.h>
#include <string>

using namespace std;

namespace SSH {
class Bignum;
class Buffer {
	public:
				Buffer 			();
				Buffer 			(char *bytes, 
							 uint32_t length);
				Buffer 			(const char *bytes, 
							 uint32_t length);
				Buffer 			(uint32_t length);
				Buffer 			(const string& str);
				Buffer 			(const Buffer &buffer);
				~Buffer 		();
		void 		append 			(char byte);
		void 		append 			(const char *bytes, 
							 uint32_t length);
		void 		append 			(uint32_t uint32);
		void 		append 			(uint64_t uint64);
		void 		append 			(const Buffer &buffer);
		void		append 			(const Buffer* b);
		void 		append 			(bool boolean);
		void 		append	 		(Bignum *bignum);

		void 		appendString 		(const char *bytes, 
							 uint32_t length);
		void 		appendString 		(const Buffer &buffer);
		void 		appendString 		(Buffer *buffer);
		void 		appendString 		(std::string str);

		unsigned char 	popByte 		();
		uint32_t 	popUint32 		();
		uint64_t 	popUint64 		();
		char *		popString 		(uint32_t *length);
		Buffer *	popStringAsBuffer 	();
		char *		popStringAsCstring	();
		string		popStringAsString	();
		Buffer *	popBytesAsBuffer	(uint32_t length);
		Bignum *	popBignum 		();

		Buffer *	hash 			() const;
		void 		truncate 		(uint32_t length);
		void 		seek 			(uint32_t index);
		void 		seekRelative		(int32_t index);
		uint32_t 	length 			() const;
		uint32_t	available		() const;
		Buffer *	remaining		() const;
		//void		compact			();
		const char *	bytes 			() const;
		char *		copyBytes 		() const;

		bool		equals			(Buffer *b) const;

		void		debug			(const string& prefix="") const;

		static Buffer *	concat			(const Buffer &a, 
							 const Buffer &b);

		static Buffer * random			(uint32_t length);
	private:
		char*		payload; /* should be allocated with malloc */
		uint32_t 	payload_length;
		uint32_t 	index; /* when we're parsing the buffer - how far in are we? */
		uint32_t 	buffer_size; /* size of allocated area */
		void	setPayload (const char *bytes, uint32_t length);
		void	setPayload (char *bytes, uint32_t length);
		void	setPayload (uint32_t length);
};

}

#endif /* SSH_BUFFER_H */
