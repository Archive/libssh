/* simple refcounting for c++ objects. its all explicit not implicit */

#ifndef SSH_REFCOUNTED_H
#define SSH_REFCOUNTED_H

#include <stdint.h>

namespace SSH {
	class Refcounted {
		public:
			Refcounted () { ref_count = 0; }
			void ref () {
				/* LOCK */
				ref_count++;
				/* UNLOCK */
			};
			void unref () {
				/* LOCK */
				ref_count--;
				/* UNLOCK */

				/* FIXME: whats the right locking for this? */
				if (ref_count <= 0) {
					delete this;
				}
			}

		private:
			uint32_t ref_count;
	};
};

#endif /* SSH_REFCOUNTED_H */
