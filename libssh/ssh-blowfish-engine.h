/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#ifndef SSH_ENGINE_BLOWFISH_H
#define SSH_ENGINE_BLOWFISH_H

#include <openssl/blowfish.h>

#include "ssh-buffer.h"

namespace SSH {

class BlowfishEngine : public Engine {
	public:
		BlowfishEngine 		(const Buffer& key,
					 const Buffer& iv);
		virtual ~BlowfishEngine	();

		virtual Buffer *encrypt (const Buffer& in);
		virtual Buffer *decrypt (const Buffer& in);

	private:
		Buffer *useCipher 	(const Buffer& in, 
					 int enc);
		BF_KEY bf_key; /* blowfish key */
		unsigned char *IV; /* initialization vector */
};

}

#endif /* SSH_ENGINE_BLOWFISH_H */

