/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include "ssh-transport.h"
#include "ssh-channel.h"
#include "ssh-session-channel.h"

namespace SSH {


SessionChannel::SessionChannel (Transport *t) :
	Channel (t)
{
	open ("session", 32765, 16384);

	/* FIXME: add "request" handler */
}


void
SessionChannel::ptyReq (const string&	term,
			uint32_t	width,
		 	uint32_t	height,
		 	uint32_t	pixel_width,
		 	uint32_t	pixel_height,
		 	const string&	terminal_modes)
{
	Buffer b;
	
	b.appendString (term);
	b.append (width);
	b.append (height);
	b.append (pixel_width);
	b.append (pixel_height);
	b.appendString (terminal_modes);

	request ("pty-req", false, b);
}

void
SessionChannel::subsystem (const string& subsystem) 
{
	Buffer b;

	b.appendString (subsystem);

	request ("subsystem", false, b);
}

void
SessionChannel::windowChange (uint32_t		 width,
			      uint32_t		 height,
			      uint32_t		 pixel_width,
			      uint32_t		 pixel_height)
{
	Buffer b;

	b.append (width);
	b.append (height);
	b.append (pixel_width);
	b.append (pixel_height);

	request ("window-change", false, b);
}

};
