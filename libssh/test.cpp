#include "ssh-socket.h"
#include "ssh-transport.h"
#include "ssh-userauth.h"
#include "ssh-bignum.h"
#include "ssh-session-channel.h"
#include "ssh-shell.h"
#include "ssh-filexfer.h"

#include <stdio.h>

#include <string>

using namespace SSH;
using namespace std;


class Tester;

class MyShell : public Shell {
	public:
		MyShell (Transport *t) : Shell (t) { }

		virtual void channelData (Channel *c, Buffer *b) {
			b->debug("data");
		}

		virtual void channelOpened (Channel  *c) {
			ptyReq ("vt100", 80, 24, 640, 480, "");
			Shell::channelOpened (c);
		}
};

class MyAuthenticator : public Authenticator {
	public:
		MyAuthenticator (const string& u, const string& p) {
			user = u;
			pass = p;
		}

		virtual ~MyAuthenticator () {}

		virtual string *getUsername () {
			return new string (user);
		}
		
		virtual string *getPassword (const string& u) { 
			return new string (pass); 
		}

		virtual void success (bool successful) {
			printf ("Auth success: %s\n", successful?"t":"f");
		}

		virtual void showBanner (const string& message, 
					 const string& language)  {
			printf ("Auth banner (%s): %s\n", language.c_str(),
					message.c_str());
		}
	private:
		string user, pass;
};


class Tester : 	public TransportListener, 
		public ChannelListener {
	public:
		Tester ();

		Socket *socket;
		Transport *transport;
		Shell *shell;
		Filexfer *xfer;

		void handlePackets ();

		/* TransportListener methods */
		void transportAuthenticated (Transport *t) {
			//shell = new MyShell (transport);
			xfer = new Filexfer (transport);
			while (!xfer->connected ()) {
				transport->handlePacket ();
			}
			printf ("FXP connected...\n");
			xfer->getResponse (xfer->opendir ("/tmp/"));
			printf ("got response\n");
		}
};

Tester::Tester() {
	printf ("Creating socket...\n");
	socket = new Socket ("localhost");

	printf ("Creating transport...\n");
	transport = new Transport (socket, new MyAuthenticator ("test", "testpass"));

	transport->addTransportListener (this);
}

void
Tester::handlePackets() {
	try {
		for (;;) {
			printf ("Handling packet...\n");
			transport->handlePacket ();
		}
	} catch (SocketException e) {
		printf ("got socket exception...\n");
	}
}

int main (int argc, char **argv) {
	Tester *t;
	try {
		t = new Tester ();
	} catch (SocketException e) {
		printf ("got socket exception in Tester constructor...\n");
		return -1;
	}

	t->handlePackets ();

	delete t;

	return 0;
}
