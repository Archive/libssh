 /* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#ifndef __SSH_PACKETHANDLER_H__
#define __SSH_PACKETHANDLER_H__

using namespace std;

namespace SSH {
	class Transport;
	class Buffer;

	class PacketHandler {
		public:
			virtual bool handlePacket (Transport*		t, 
						   const Buffer& 	buffer) = 0;
	};
};

#endif /* __SSH_PACKETHANDLER_H__ */
