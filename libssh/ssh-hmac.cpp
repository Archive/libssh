/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include "ssh-hmac.h"
#include "ssh-utils.h"

#include <string>
#include <openssl/sha.h>
#include <openssl/hmac.h>

using namespace std;

namespace SSH {

/* private classes */
namespace HMacs {
	class None : public HMac {
		public:
			virtual uint32_t length () const { return 0; };
			virtual Buffer* operator()(uint32_t sequence, 
					const Buffer& packet) {
				return new Buffer(0);
			};
	};

	class SHA1 : public HMac {
		friend class HMac;
		public:
			virtual uint32_t length () const;
			virtual Buffer* operator()(uint32_t sequence, const Buffer& packet);
			virtual ~SHA1 ();

		private:
			SHA1 (const Buffer& k, uint32_t dlen);
			EVP_MD* evp_md;
			uint32_t digest_length;
			Buffer key;
	};
};

/* SSH::HMac */
string
HMac::availableAlgorithms ()
{
	//return string("hmac-sha1-96,hmac-sha1");
	return string("hmac-sha1-96");
}

HMac *
HMac::forAlgorithm (const string& algorithm, const Buffer& k)
{
	if (algorithm == "hmac-sha1-96") {
		return new HMacs::SHA1 (k, 12);
	} else if (algorithm == "hmac-sha1") {
		return new HMacs::SHA1 (k, 20);
	} else {
		return NULL;
	}
}

/* from draft-ietf-secsh-transport-09.txt 5.1
 * The chosen MAC algorithm MUST be the first algorithm on the client's
 * list that is also on the server's list.
 */
string
HMac::chooseAlgorithm (const string& client_algorithms,
		       const string& server_algorithms)
{
	StringList client(client_algorithms);
	StringList server(server_algorithms);

	StringList::iterator iter;

	for (iter = client.begin(); iter != client.end(); iter++) {
		if (server.find (*iter) >= 0) {
			return *iter;
		}
	}

	return string("");
}

HMac* 
HMac::none ()
{
	return new HMacs::None ();
}

/* SSH::HMacs::SHA1 */
HMacs::SHA1::SHA1 (const Buffer& k, uint32_t dlen) :
	digest_length(dlen),
	key(k)
{
	evp_md = EVP_sha1 ();
}

uint32_t
HMacs::SHA1::length () const
{
	return digest_length;
}

Buffer*
HMacs::SHA1::operator()(uint32_t sequence, const Buffer& packet)
{
	Buffer in;
	unsigned char md [EVP_MAX_MD_SIZE];
	unsigned int md_len;

	in.append (sequence);
	in.append (packet);

	HMAC (evp_md, key.bytes (), key.length (),
			(const unsigned char *)in.bytes (), 
			in.length (), md, &md_len);

	Buffer *out = new Buffer ((const char*)&md[0], digest_length);

	return out;
}


HMacs::SHA1::~SHA1 ()
{
	/* FIXME: do I need to free evp_md? */
}

};
