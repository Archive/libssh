/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#ifndef SSH_PROTOCOL_H
#define SSH_PROTOCOL_H

/* message ids */
#define SSH_MSG_DISCONNECT			1
#define SSH_MSG_IGNORE				2
#define SSH_MSG_UNIMPLEMENTED			3
#define SSH_MSG_DEBUG				4
#define SSH_MSG_SERVICE_REQUEST 		5
#define SSH_MSG_SERVICE_ACCEPT 			6

#define SSH_MSG_KEXINIT 			20
#define SSH_MSG_NEWKEYS 			21

#define SSH_MSG_KEXDH_INIT 			30
#define SSH_MSG_KEXDH_REPLY 			31

#define SSH_MSG_USERAUTH_REQUEST 		50
#define SSH_MSG_USERAUTH_FAILURE 		51
#define SSH_MSG_USERAUTH_SUCCESS 		52
#define SSH_MSG_USERAUTH_BANNER 		53

#define SSH_MSG_GLOBAL_REQUEST			80
#define SSH_MSG_REQUEST_SUCCESS			81
#define SSH_MSG_REQUEST_FAILURE			82

#define SSH_MSG_CHANNEL_MIN			90
#define SSH_MSG_CHANNEL_MAX			100
#define SSH_MSG_CHANNEL_OPEN			90
#define SSH_MSG_CHANNEL_OPEN_CONFIRMATION	91
#define SSH_MSG_CHANNEL_OPEN_FAILURE		92
#define SSH_MSG_CHANNEL_WINDOW_ADJUST		93
#define SSH_MSG_CHANNEL_DATA			94
#define SSH_MSG_CHANNEL_EXTENDED_DATA		95
#define SSH_MSG_CHANNEL_EOF			96
#define SSH_MSG_CHANNEL_CLOSE			97
#define SSH_MSG_CHANNEL_REQUEST			98
#define SSH_MSG_CHANNEL_SUCCESS			99
#define SSH_MSG_CHANNEL_FAILURE			100


/* connection open failue message ids */
#define SSH_OPEN_ADMINISTRATIVELY_PROHIBITED	1
#define SSH_OPEN_CONNECT_FAILED			2
#define SSH_OPEN_UNKNOWN_CHANNEL_TYPE		3
#define SSH_OPEN_RESOURCE_SHORTAGE		4

/* extended connection data types */
#define SSH_EXTENDED_DATA_STDERR		1

/* service ids */
#define SERVICE_USERAUTH 	"ssh-userauth"
#define SERVICE_CONNECTION 	"ssh-connection"

/* subsystem ids */
#define SUBSYSTEM_FILEXFER	"sftp"

/* filexfer commands */
#define SSH_FXP_INIT                1
#define SSH_FXP_VERSION             2
#define SSH_FXP_OPEN                3
#define SSH_FXP_CLOSE               4
#define SSH_FXP_READ                5
#define SSH_FXP_WRITE               6
#define SSH_FXP_LSTAT               7
#define SSH_FXP_FSTAT               8
#define SSH_FXP_SETSTAT             9
#define SSH_FXP_FSETSTAT           10
#define SSH_FXP_OPENDIR            11
#define SSH_FXP_READDIR            12
#define SSH_FXP_REMOVE             13
#define SSH_FXP_MKDIR              14
#define SSH_FXP_RMDIR              15
#define SSH_FXP_REALPATH           16
#define SSH_FXP_STAT               17
#define SSH_FXP_RENAME             18
#define SSH_FXP_STATUS            101
#define SSH_FXP_HANDLE            102
#define SSH_FXP_DATA              103
#define SSH_FXP_NAME              104
#define SSH_FXP_ATTRS             105
#define SSH_FXP_EXTENDED          200
#define SSH_FXP_EXTENDED_REPLY    201

/* filexfer open modes */
#define SSH_FXF_READ	0x00000001
#define SSH_FXF_WRITE	0x00000002
#define SSH_FXF_APPEND	0x00000004
#define SSH_FXF_CREAT	0x00000008
#define SSH_FXF_TRUNC	0x00000010
#define SSH_FXF_EXCL	0x00000020

/* filexfer attribute flags */
#define SSH_FILEXFER_ATTR_SIZE          0x00000001
#define SSH_FILEXFER_ATTR_UIDGID        0x00000002
#define SSH_FILEXFER_ATTR_PERMISSIONS   0x00000004
#define SSH_FILEXFER_ATTR_ACMODTIME     0x00000008
#define SSH_FILEXFER_ATTR_EXTENDED      0x80000000

/* filxfer status codes */
#define SSH_FX_OK                            0
#define SSH_FX_EOF                           1
#define SSH_FX_NO_SUCH_FILE                  2
#define SSH_FX_PERMISSION_DENIED             3
#define SSH_FX_FAILURE                       4
#define SSH_FX_BAD_MESSAGE                   5
#define SSH_FX_NO_CONNECTION                 6
#define SSH_FX_CONNECTION_LOST               7
#define SSH_FX_OP_UNSUPPORTED                8


#define SSH_FXP_PROTOCOL_VERSION	3



#endif /* SSH_PROTOCOL_H */
