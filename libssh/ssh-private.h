/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#ifndef SSH_PRIVATE_H
#define SSH_PRIVATE_H

#if 0
#include "ssh-transport.h"
#include "ssh-userauth.h"
#include "ssh-channel.h"

/* private APIs */
void	 ssh_transport_send_packet 	(SshTransport 	*t,
					 SshBuffer 	*p);
void	 ssh_transport_send_SERVICE_REQUEST(SshTransport 	*t, 
					 gchar 		*service);
gboolean sconnection_packet_handler 	(SshTransport 	*t, 
					 SshBuffer *p);
#endif

#endif /* SSH_PRIVATE_H */
