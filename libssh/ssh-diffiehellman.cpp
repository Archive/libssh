/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include "ssh-bignum.h"
#include "ssh-diffiehellman.h"

#include <string>

#include <assert.h>

using namespace std;

namespace SSH {

DiffieHellman::DiffieHellman ()
{
	char *group1_hex = 
	  "FFFFFFFF" "FFFFFFFF" "C90FDAA2" "2168C234" "C4C6628B" "80DC1CD1"
	  "29024E08" "8A67CC74" "020BBEA6" "3B139B22" "514A0879" "8E3404DD"
	  "EF9519B3" "CD3A431B" "302B0A6D" "F25F1437" "4FE1356D" "6D51C245"
	  "E485B576" "625E7EC6" "F44C42E9" "A637ED6B" "0BFF5CB6" "F406B7ED"
	  "EE386BFB" "5A899FA5" "AE9F2411" "7C4B1FE6" "49286651" "ECE65381"
	  "FFFFFFFF" "FFFFFFFF";
	char *gen = "2";

	dh = DH_new ();
	assert (dh != NULL);

	/* FIXME: check these return vals */
        BN_hex2bn(&dh->p, group1_hex);
	BN_hex2bn(&dh->g, gen);

        do {
                dh->priv_key = BN_new();
		assert (dh->priv_key != NULL);

                BN_rand(dh->priv_key, 1024, 0, 0);

                DH_generate_key(dh);

        } while (!publicKeyValid());
}

DiffieHellman::~DiffieHellman ()
{
	DH_free (dh);
}

Bignum *
DiffieHellman::publicKey () const 
{
	return new Bignum (dh->pub_key);
}

/* this function was ripped from OpenSSH (dh_pub_is_valid) */
bool
DiffieHellman::publicKeyValid () const
{
        int i;
        int n = BN_num_bits(dh->pub_key);
        int bits_set = 0;

        if (dh->pub_key->neg) {
                return false;
        }
        for (i = 0; i <= n; i++) {
                if (BN_is_bit_set (dh->pub_key, i)) {
                        bits_set++;
		}
	}

        /* if g==2 and bits_set==1 then computing log_g(dh->pub_key) is trivial */
        if (bits_set > 1 && (BN_cmp(dh->pub_key, dh->p) == -1)) {
                return true;
	}

        return false;
}

Bignum *
DiffieHellman::computeKey (Bignum *key) const
{
        unsigned char *buf = (unsigned char *)malloc (DH_size (dh));
        int len = DH_compute_key (buf, key->bignum, dh);

	Bignum *bn = new Bignum ((char *)buf, len);

	free (buf);

	return bn;
}

};
