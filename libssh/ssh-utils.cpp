/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 *
 * A bunch of utility classes and functions that don't depend on anything else
 * in libssh.
 */

#include "ssh-utils.h"

#include <stdio.h>

namespace SSH {


StringList::StringList (const string& str, char separator)
{
	int start = 0;
	int end = 0;

	while (end < str.length ()) {
		end = str.find (separator, start);

		if (end == string::npos) {
			/* separator not found */
			end = str.length ();
		}

		push_back (str.substr (start, end-start));

		start = end + 1;
	}

}

int 
StringList::find (const string& str)
{
	StringList::iterator iter;
	int i = 0;

	for (iter = begin(); iter != end(); iter++) {
		if (*iter == str) {
			return i;
		}
		i++;
	}

	return -1;
}

};
