/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#ifndef _
#define _(X) (X)
#endif

#include <string.h>

#include "ssh-transport.h"
#include "ssh-channel.h"
#include "ssh-protocol.h"

namespace SSH {

Channel::Channel (Transport* t) :
		eof_pending(false),
		eof_sent(false),
		eof_received(false),
		close_pending(false),
		close_sent(false),
		close_received(false)
{ 
	pending = NULL;
	transport = t; 
	transport->addPacketHandler (this);
}

Channel::~Channel ()
{
	transport->removePacketHandler (this);

	/* FIXME: free other shit */
}

void
Channel::OPEN_CONFIRMATION (Buffer *p)
{
	printf ("[YAK] got open confirmation\n");

	their_channel = p->popUint32 ();
	send_window = p->popUint32 ();
	send_packet_max = p->popUint32 ();

	printf ("[YAK] their_channel = %d\n", their_channel);
	printf ("[YAK] send_window = %d\n", send_window);
	printf ("[YAK] send_packet_max = %d\n", send_packet_max);

	/* inform listeners */
	ChannelListeners::iterator	iter;
	for (iter = channelListeners.begin(); 
			iter != channelListeners.end(); 
			iter++) {
		(*iter)->channelOpened (this);
	}

	/* write pending data */
	tryToWritePending ();
}

void
Channel::OPEN_FAILURE (Buffer *p)
{
	printf ("[YAK] got open failure\n");

	/* inform listeners */
	ChannelListeners::iterator	iter;
	for (iter = channelListeners.begin(); 
			iter != channelListeners.end(); 
			iter++) {
		(*iter)->channelOpenFailure (this);
	}
}

void
Channel::DATA (Buffer *p)
{
	Buffer	*datab;

	if (close_received || eof_received) {
		/* they shouldn't still be sending us stuff */
		/* lets ignore it */
		return;
	}

	datab = p->popStringAsBuffer ();
	windowAdjust (datab->length ());

	ChannelListeners::iterator	iter;
	for (iter = channelListeners.begin(); 
			iter != channelListeners.end(); 
			iter++) {
		(*iter)->channelData (this, datab);
	}

	delete datab;
}

void
Channel::EXTENDED_DATA (Buffer *p)
{
	uint32_t 	 type;
	Buffer 	*data;

	if (close_received || eof_received) {
		/* they shouldn't still be sending us stuff */
		/* lets ignore it */
		return;
	}

	type = p->popUint32 ();
	data = p->popStringAsBuffer ();
	windowAdjust (data->length ());

	ChannelListeners::iterator	iter;
	for (iter = channelListeners.begin(); 
			iter != channelListeners.end(); 
			iter++) {
		(*iter)->channelExtendedData (this, type, data);
	}

	delete data;
}

void
Channel::WINDOW_ADJUST (Buffer *p)
{
	uint32_t		 bytes;

	bytes = p->popUint32 ();

	send_window += bytes;

	printf ("[YAK] window adjust: %d bytes, send_window = %d\n", bytes, send_window);
	
	/* cool, so we're allowed to write more - lets try */
	tryToWritePending ();
}

void
Channel::CHANNEL_EOF (Buffer *p)
{

	printf ("Channel::EOF\n");

	eof_received = true;

	ChannelListeners::iterator	iter;
	for (iter = channelListeners.begin(); 
			iter != channelListeners.end(); 
			iter++) {
		(*iter)->channelEOF (this);
	}
}

void
Channel::CLOSE (Buffer *p)
{
	printf ("Channel::CLOSE\n");

	close_received = true;

      	/* When either party wishes to terminate the channel, it sends
      	 * SSH_MSG_CHANNEL_CLOSE.  Upon receiving this message, a party MUST
      	 * send back a SSH_MSG_CHANNEL_CLOSE unless it has already sent this
      	 * message for the channel.  The channel is considered closed for a
      	 * party when it has both sent and received SSH_MSG_CHANNEL_CLOSE,
      	 * and the party may then reuse the channel number.  A party MAY send
      	 * SSH_MSG_CHANNEL_CLOSE without having sent or received
      	 * SSH_MSG_CHANNEL_EOF.
      	 */

	if (!close_pending && !close_sent) {
		/* We got a CLOSE and we haven't tried to send one yet. Lets
		 * try.
		 */
		close_pending = true;
		tryToWritePending ();
	}

	/* inform listeners */
	ChannelListeners::iterator	iter;
	for (iter = channelListeners.begin(); 
			iter != channelListeners.end(); 
			iter++) {
		(*iter)->channelClosed (this);
	}
}

void
Channel::REQUEST (Buffer *p)
{

}

bool
Channel::handlePacket (Transport *t, const Buffer& b)
{
	unsigned char type;
	Buffer *p = new Buffer (b);
	uint32_t channel_id;

	/* is it a channel message? */
	p->seek (0);
	type = p->popByte ();
	if (type < SSH_MSG_CHANNEL_MIN || type > SSH_MSG_CHANNEL_MAX) {
		return false;
	}

	/* is it for this channel? */
	channel_id = p->popUint32 ();
	if (channel_id != our_channel) {
		return false;
	}

	switch (type) {
		case SSH_MSG_CHANNEL_OPEN_CONFIRMATION:
			OPEN_CONFIRMATION (p);
			return true;
			break;

		case SSH_MSG_CHANNEL_OPEN_FAILURE:
			OPEN_FAILURE (p);
			return true;
			break;

		case SSH_MSG_CHANNEL_DATA:
			DATA (p);
			return true;
			break;

		case SSH_MSG_CHANNEL_EXTENDED_DATA:
			EXTENDED_DATA (p);
			return true;
			break;

		case SSH_MSG_CHANNEL_WINDOW_ADJUST:
			WINDOW_ADJUST (p);
			return true;
			break;

		case SSH_MSG_CHANNEL_EOF:
			CHANNEL_EOF (p);
			return true;
			break;

		case SSH_MSG_CHANNEL_CLOSE:
			CLOSE (p);
			return true;
			break;

		case SSH_MSG_CHANNEL_REQUEST:
			REQUEST (p);
			return true;
			break;
	}

	return false;
}

/* this should really only be used by subclasses. */
void
Channel::open (const string& chan_type, uint32_t max_window, 
		uint32_t max_packet)
{
	/* FIXME: make sure this only gets called once */

	our_channel = transport->addChannel (this);
	channel_type = chan_type;
	recv_window_max = max_window;
	recv_window = recv_window_max;
	recv_packet_max = max_packet;
	recv_window = max_window;
	send_window = 0; /* we don't know this yet */
	send_packet_max = 0; /* we don't know this yet */

	Buffer p;
	p.append ((char)SSH_MSG_CHANNEL_OPEN);
	p.appendString (channel_type);
	p.append (our_channel);
	p.append (recv_window);
	p.append (recv_packet_max);
	transport->sendPacket (p);
}

void
Channel::actuallyWrite (Buffer *b)
{
	Buffer p (b->length() + 32);

	printf ("[YAK] actuallyWrite (%d bytes)\n", b->length());

	p.appendString (b);
	sendPacket (SSH_MSG_CHANNEL_DATA, p);
}

#define MIN2(x,y) ((x)<(y)?(x):(y))
#define MIN3(x,y,z) MIN2(MIN2((x),(y)),(z))

void
Channel::tryToSendEOFClose ()
{
	if (pending == NULL || pending->length() <=0 ) {
		/* nothing to write */

		if (eof_pending) {
			sendPacket (SSH_MSG_CHANNEL_EOF);
			eof_pending = false;
			eof_sent = true;
		}

		if (close_pending) {
			sendPacket (SSH_MSG_CHANNEL_CLOSE);
			close_pending = false;
			close_sent = true;
		}
	}
}

void
Channel::tryToWritePending ()
{
	uint32_t packet_length;
	Buffer *new_pending;
	Buffer *p;

	printf ("[YAK] ssh_channel_try_to_write_pending (send_window = %d)\n", send_window);

	tryToSendEOFClose ();

	if (pending==NULL || pending->length() <= 0) {
		/* I guess theres nothing to write */

		/* But is there a pending EOF? */
		return;
	}

	if (pending == NULL) {
		pending = new Buffer();
	}

	while (send_window > 0) {
		packet_length = MIN3(send_window, send_packet_max, 
				pending->length ());
		if (packet_length <= 0) {
			break;
		}
		p = new Buffer ((const char *)pending->bytes(), packet_length);
		new_pending = new Buffer ((const char *)pending->bytes() + 
				packet_length,
				pending->length() - packet_length);

		delete pending;
		pending = new_pending;
		send_window -= packet_length;
		actuallyWrite (p);
		delete p;
	}

	tryToSendEOFClose ();
}

void
Channel::write (const Buffer& b)
{
	printf ("Channel::write length=%d\n", b.length());

	if (eof_pending || eof_sent) {
		throw new ChannelEOFException ();
	}

	if (close_pending || close_sent) {
		throw new ChannelClosedException ();
	}

	if (pending == NULL) {
		pending = new Buffer (b);
	} else {
		Buffer *new_pending = Buffer::concat (*pending, b);
		delete pending;
		pending = new_pending;
	}

	tryToWritePending ();
}

void
Channel::write (const string& str)
{
	Buffer tmp(str);

	write (tmp);
}

void
Channel::windowAdjust (uint32_t size)
{
	Buffer p;

	if (close_pending || close_sent) {
		throw new ChannelClosedException ();
	}

	p.append (size);
	sendPacket (SSH_MSG_CHANNEL_WINDOW_ADJUST, p);
}

void
Channel::request (const string&	type,
		  bool 		want_reply, 
		  const Buffer&	extra)
{
	Buffer p;

	if (close_pending || close_sent) {
		throw new ChannelClosedException ();
	}

	printf ("[YAK] channel request type=\"%s\"\n", type.c_str());
	printf ("[YAK] their_channel=%d\n", their_channel);

	p.appendString (type);
	p.append (want_reply);
	p.append (extra); /* as bytes not string */
	sendPacket (SSH_MSG_CHANNEL_REQUEST, p);
}

void
Channel::request (const string&	type, 
		  bool 	 	want_reply)
{
	Buffer tmp (0);

	request (type, want_reply, tmp);
}

void
Channel::request (const string&	type, 
		  bool		want_reply, 
		  const string&	string)
{
	Buffer tmp;

	tmp.appendString (string);

	request (type, want_reply, tmp);
}

void
Channel::eof ()
{
	/* From draft-ietf-secsh-connect-11.txt 3.3:
	 * When a party will no longer send more data to a channel, it SHOULD
	 * send SSH_MSG_CHANNEL_EOF.
	 */

	if (eof_pending || eof_sent) {
		throw new ChannelEOFException ();
	}

	if (close_pending || close_sent) {
		throw new ChannelClosedException ();
	}

	eof_pending = true;
	tryToWritePending ();
}

void
Channel::close ()
{
	if (close_pending || close_sent) {
		throw new ChannelClosedException ();
	}

	eof_pending = true;
	tryToWritePending ();
}
			
void	
Channel::sendPacket (char type, 
		     const Buffer& b)
{
	Buffer p;

	p.append (type);
	p.append (their_channel);
	p.append (b);
	transport->sendPacket (p);
}

void
Channel::sendPacket (char type)
{
	Buffer p(0);
	sendPacket (type, p);
}

void
Channel::addChannelListener (ChannelListener *ph)
{
	channelListeners.push_back (ph);
}

void
Channel::removeChannelListener (ChannelListener *ph)
{
	ChannelListeners::iterator	iter;

	for (iter = channelListeners.begin();
			iter != channelListeners.end();
			iter++) {
		if (*iter == ph) {
			channelListeners.erase (iter);
			return;
		}
	}
}

};
