/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 *
 * A bunch of utility classes and functions that don't depend on anything else
 * in libssh.
 */

#ifndef SSH_UTILS_H
#define SSH_UTILS_H

#include <string>
#include <vector>

using namespace std;

namespace SSH {
	class StringList : public vector<string> {
		public:
			StringList (const string& str, char separator=',');
			int find (const string& str);
	};

} /* namespace SSH */


#endif /* SSH_UTILS_H */
