/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include "ssh-engine.h"
#include "ssh-blowfish-engine.h"

namespace SSH {

	BlowfishEngine::BlowfishEngine (const Buffer& key, const Buffer& iv)
	{
		IV = (unsigned char *) iv.copyBytes ();
		BF_set_key (&bf_key, key.length (),
				(unsigned char *)key.bytes ());
	}

	BlowfishEngine::~BlowfishEngine ()
	{
		free (IV);
	}

	Buffer *
	BlowfishEngine::useCipher (const Buffer& in, int enc)
	{
		Buffer *out;
		char *outbytes;

		outbytes = (char *)malloc (in.length ());

		BF_cbc_encrypt ((const unsigned char *)in.bytes (), 
				(unsigned char *)outbytes, 
				in.length (), &bf_key, IV, enc);

		out = new Buffer (outbytes, in.length ());

		return out;
	}

	Buffer *
	BlowfishEngine::encrypt (const Buffer& in)
	{
		return useCipher (in, BF_ENCRYPT);
	}

	Buffer *
	BlowfishEngine::decrypt (const Buffer& in)
	{
		return useCipher (in, BF_DECRYPT);
	}

}
