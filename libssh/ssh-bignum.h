/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

/*
 * This class wraps bignum so we can swap out OpenSSL for something else at
 * a later date.
 */

#ifndef __SSH_BIGNUM_H__
#define __SSH_BIGNUM_H__

#include <string>
#include <inttypes.h>
#include <openssl/bn.h>

#include "ssh-buffer.h"

using namespace std;

namespace SSH {
	class DiffieHellman;
	class Bignum {
		friend class DiffieHellman;
		public:
			Bignum 	(const char*	bytes, 
				 uint32_t	length);
			Bignum	(const string&	str);
			Bignum	();

			~Bignum	();
			Buffer *binary ();
			void debug (const string& str);

			void test ();

		private:
			Bignum::Bignum (BIGNUM *bn);
			BIGNUM *bignum;

	};

};

#endif /* __SSH_BIGNUM_H__ */
