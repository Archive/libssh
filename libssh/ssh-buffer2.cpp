/* a new set of buffer classes */

#include <stdint.h>
#include <string>
#include <vector>
#include <stdio.h> // for printf in Buffer2::debug
#include <netdb.h> // for htonl

#include "ssh-buffer2.h"

using namespace std;

namespace SSH { 

int x=0;
	 
void 
Buffer2::debug (const string& prefix) const 
{

	uint32_t i, j;
	uint32_t rowlen = 16;
	const char *c_prefix = prefix.c_str();

	uint32_t l = getLength ();
	char *d = new char[l];
	getBytes (d);

	for (i=0; i<l; i+=rowlen) {
		printf ("%12.12s ", c_prefix);
		for (j=0; j<rowlen && (i+j)<l; j++) {
			printf ("%02X ", (unsigned char)d[i+j]);
		}

		for (; j<rowlen; j++) {
			printf ("   ");
		}

		for (j=0; j<rowlen && (i+j)<l; j++) {
			if (isprint (d[i+j])) {
				printf ("%c", (unsigned char)d[i+j]);
			} else {
				printf (".");
			}
		}

		printf ("\n"); 
	} 
	delete[] d;
}


uint32_t 
ListBuffer::getLength() const {
	uint32_t len = 0;
	vector<Buffer2*const>::iterator iter;

	for (iter=children.begin();
			iter!=children.end();
			iter++) {
		len += (*iter)->getLength();
	}
	return len;
}

void 
ListBuffer::getBytes(char *b) const {
	char *ptr = b;
	vector<Buffer2*const>::iterator iter;

	for (iter=children.begin();
			iter!=children.end();
			iter++) {
		(*iter)->getBytes(ptr);
		ptr += (*iter)->getLength();
	}
}

ListBuffer::~ListBuffer () {
	vector<Buffer2*const>::iterator iter;

	for (iter=children.begin();
			iter!=children.end();
			iter++) {
		(*iter)->unref ();
	}
}

};
