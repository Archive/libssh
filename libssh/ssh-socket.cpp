/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "ssh-socket.h"

namespace SSH {

Socket::Socket (const std::string& host, int port)
{
	struct hostent *he;
	struct sockaddr_in sin;

	/* create the socket */
	filedes = socket (PF_INET, SOCK_STREAM, 0);
	if (filedes == -1) {
		throw new SocketException ("socket");
	}

	/* look up the host name */
	he = gethostbyname (host.c_str ());
	if (he == NULL) {
		throw new SocketException ("gethostbyname");
	}

	/* connect to the server */
        sin.sin_family = he->h_addrtype;
        sin.sin_addr = * (struct in_addr *) he->h_addr;
        sin.sin_port = htons (port);
	if (connect (filedes, (struct sockaddr *)&sin, sizeof (sin)) == -1) {
		throw new SocketException ("connect");
	}

}

void
Socket::read (char	*buffer,
	      int	 length,
	      int	*length_read)
{
	*length_read = ::read (filedes, buffer, length);

	if (*length_read <= 0) {
		printf ("read failed - returned %d: %s\n", *length_read, strerror (errno));
		throw new SocketException ("read");
	}
}

void
Socket::write (const char	*buffer,
	       int		 length,
	       int		*length_written)
{
	*length_written = ::write (filedes, buffer, length);

	if (*length_written < 0) {
		printf ("write failed :(\n");
		throw new SocketException ("write");
	}
}

} /* namespace SSH */


