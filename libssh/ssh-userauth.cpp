/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

/* HACK! */
#ifndef _
#define _(X) (X)
#endif

#include "ssh-userauth.h"
#include "ssh-buffer.h"
#include "ssh-protocol.h"

#include <string>

using namespace std;

namespace SSH {

Userauth::Userauth (Transport *t, Authenticator *a)
{
	transport = t;
	authenticator = a;

	printf ("Userauth::Userauth\n");

	transport->addPacketHandler (this);

	transport->sendSERVICE_REQUEST (SERVICE_USERAUTH);
}

Userauth::~Userauth ()
{
	transport->removePacketHandler (this);
}

bool
Userauth::handlePacket (Transport *t, const Buffer& b)
{
	unsigned char type;
	Buffer *p = new Buffer (b);

	/* already seeked p->seek(0);*/
	type = p->popByte ();

	switch (type) {
		case SSH_MSG_USERAUTH_SUCCESS:
			/* inform the app */
			authenticator->success (true);
			transport->authenticated ();

			return true;
			break;

		case SSH_MSG_SERVICE_ACCEPT:
			return recvSERVICE_ACCEPT (p);
			break;


	}

	return false;
}

void
Userauth::authPassword ()
{
	Buffer p;

	string *username = authenticator->getUsername ();
	if (username == NULL) {
		return; /* FIXME: report failure */
	}

	string *password = authenticator->getPassword (*username);
	if (password == NULL) {
		return; /* FIXME: report failure */
	}

	p.append ((char) SSH_MSG_USERAUTH_REQUEST);
	p.appendString (*username);
	p.appendString ("ssh-connection");
	p.appendString ("password");
	p.append (false);
	p.appendString (*password);

	transport->sendPacket (p);
}

/* move to the next step of user authentication */
void
Userauth::nextStep ()
{
	state++;

	switch (state) {
		case Userauth::STATE_PASSWORD:
			authPassword ();
#if 0 /* dunno... */
			} else {
				ssh_userauth_next_step (t);
			}
#endif
			break;

		case Userauth::STATE_FAILED:
		default:
			authenticator->success (false);
			break;
	}
}

bool
Userauth::recvSERVICE_ACCEPT (Buffer *p)
{
	char *service_name = p->popStringAsCstring ();

	if (!strcmp (service_name, SERVICE_USERAUTH)) {
		nextStep ();
		free (service_name);
		return true;
	} else {
		free (service_name);
		return false;
	}
}

};
