/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include "ssh-buffer.h"

#ifndef SSH_ENGINE_H
#define SSH_ENGINE_H

namespace SSH {

	class Engine {
		public:
			virtual Buffer *encrypt (const Buffer& in) = 0;
			virtual Buffer *decrypt (const Buffer& in) = 0;
	};

} /* namespace SSH */


#endif /* SSH_ENGINE_H */
