/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */


#ifndef __SSH_EXEC_H__
#define __SSH_EXEC_H__

#include "ssh-channel.h"
#include "ssh-session-channel.h"
#include "ssh-buffer.h"
#include "ssh-transport.h"

#include <string>

using namespace std;

namespace SSH {
	class Exec :	public SessionChannel, 
			protected ChannelListener {
		public:
			Exec 				(Transport*	t,
							 const string&	cmd);

		protected:
			virtual void channelOpened 	(Channel  *c);

		private:
			string command; /* FIXME: will this be automagicall destructed? */
	};
};
 


#endif /* __SSH_EXEC_H__ */

