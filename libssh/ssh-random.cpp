/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include <openssl/rand.h>
#include <string.h>

#include "ssh-random.h"

void
SSH::Random::randomize (char *buffer, int length)
{
	/* FIXME: seed buffer for systems without /dev/urandom */

	RAND_bytes ((unsigned char *)buffer, length);
}
