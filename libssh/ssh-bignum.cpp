/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include "ssh-bignum.h"

#include <string>

#include <assert.h>

using namespace std;

namespace SSH {

Bignum::Bignum (const char* bytes, uint32_t length)
{
	bignum = BN_new ();

	BN_bin2bn ((unsigned char*)bytes, length, bignum);
}

Bignum::Bignum	(const string&	str)
{
	bignum = BN_new ();

	BN_bin2bn ((unsigned char*)str.c_str (), str.length (), bignum);
}

Bignum::Bignum ()
{
	bignum = BN_new ();
}

Bignum::Bignum (BIGNUM *bn) {
	bignum = bn;
}

Bignum::~Bignum ()
{
	BN_free (bignum);
}

/* return this Bignum encoded as an SSH mpint */
Buffer *
Bignum::binary () {
	uint32_t length = BN_num_bytes (bignum) + 1;
	unsigned char *string = (unsigned char *)malloc (length);
	uint32_t actual_length, hasnohigh;
	uint32_t i, carry;
	unsigned char *uc;

	string[0] = 0; /* do not underestimate the importance of this line */

	actual_length = BN_bn2bin (bignum, string+1);

	assert (actual_length == length-1);

	hasnohigh = (string[1] & 0x80) ? 0 : 1;

	if (bignum->neg) {
		uc = string;
		for (i = length-1, carry = 1; i >= 0; i--) {
			uc[i] ^= 0xFF;
			if (carry) {
				carry = !++uc[i];
			}
		}
	}

	Buffer *buf = new Buffer();
	buf->appendString ((const char *)string+hasnohigh, length-hasnohigh);

	free (string);

	return buf;
}

void
Bignum::debug (const string& str)
{
	fprintf(stdout, "%s: ", str.c_str());
	BN_print_fp(stdout, bignum);
	fprintf(stdout, "\n");
}

void
Bignum::test () {
	Bignum *p,*q;

	p = new Bignum ("8C72A7334BAFF03CE5CFB5FF80F0F23308EDA5432C8332BA0FA71CD1615B6DC846B34C0B73F2D00B99390CA1ED74F6CF5C95FF9FA4F5FC8DC2FF90014039FA521BDC3F15957A2AF67DE3C9BACC2066FB5189FF374015FCA8D509E80F40FFA2E20D2EA0C2611AF229E1875FD790182E15E2F8B6A2C78C17EC295EDEAF8D0274DD");

	Buffer b;
	b.append (p);
	q = b.popBignum ();

	p->debug ("p");
	q->debug ("p");
}

};
