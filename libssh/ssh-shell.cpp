/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include "ssh-shell.h"
#include "ssh-session-channel.h"
#include "ssh-transport.h"
#include "ssh-channel.h"

namespace SSH {

Shell::Shell (Transport *t) : SessionChannel (t) {
	addChannelListener (this);
}

void 
Shell::channelOpened (Channel  *c) {
	request ("shell", true);
}

};
