#ifndef SSH_BUFFER2_PRIVATE_H
#define SSH_BUFFER2_PRIVATE_H

#include <stdint.h>
#include <string>
#include <vector>
#include <stdio.h> // for printf in Buffer2::debug
#include <netdb.h> // for htonl

#include "ssh-refcounted.h"
#include "ssh-buffer2.h"

using namespace std;

namespace SSH { 
	class ByteBuffer : public Buffer2 {
		public:
			virtual ~ByteBuffer () {};
			ByteBuffer (unsigned char byte) { data = byte; }
			virtual uint32_t getLength() const { return 1; }
			virtual void getBytes(char *b) const 
					{ *b = (char)data; }
		private:
			unsigned char data;
	};
	
	class BooleanBuffer : public Buffer2 {
		public:
			virtual ~BooleanBuffer () {};
			BooleanBuffer (bool boolean) { data = boolean; }
			virtual uint32_t getLength() const { return 1; }
			virtual void getBytes(char *b) const 
					{ *b = data?'\1':'\0'; }
		private:
			bool data;
	};

	class Uint32Buffer : public Buffer2 {
		public:
			virtual ~Uint32Buffer () {};
			Uint32Buffer (uint32_t uint32) { data = htonl(uint32); }
			virtual uint32_t getLength() const { return 4; }
			virtual void getBytes(char *b) const {
				memcpy (b, (const char*)&data, getLength ());
			}
		private:
			uint32_t data;
	};

	class Uint64Buffer : public Buffer2 {
		public:
			virtual ~Uint64Buffer () {};
			Uint64Buffer (uint64_t i) { 
				data = ((((i) & 0xff00000000000000ull) >> 56) |\
			   		(((i) & 0x00ff000000000000ull) >> 40) |\
			   		(((i) & 0x0000ff0000000000ull) >> 24) |\
			   		(((i) & 0x000000ff00000000ull) >> 8) |\
			   		(((i) & 0x00000000ff000000ull) << 8) |\
			   		(((i) & 0x0000000000ff0000ull) << 24) |\
			   		(((i) & 0x000000000000ff00ull) << 40) |\
			   		(((i) & 0x00000000000000ffull) << 56));
			}
			virtual uint32_t getLength() const { return 8; }
			virtual void getBytes(char *b) const {
				memcpy (b, (const char*)&data, getLength ());
			}
		private:
			uint64_t data;
	};

	class StringBuffer : public Buffer2 {
		public:
			virtual ~StringBuffer () { delete[] data; };
			StringBuffer (string str) { 
				length = str.length ();
				data = new char[length];
				memcpy (data, str.c_str(), length);
			}
			virtual uint32_t getLength() const { return length+4; }
			virtual void getBytes(char *b) const {
				uint32_t nworder = htonl (length);
				memcpy (b, (const char*)&nworder, 4);
				memcpy (b+4, (const char*)data, length);
			}
		private:
			char*		data;
			uint32_t	length;
	};

}

#endif /* SSH_BUFFER2_PRIVATE_H */
