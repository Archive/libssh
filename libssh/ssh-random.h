/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#ifndef SSH_RANDOM_H
#define SSH_RANDOM_H

namespace SSH {
	class Random {
		public:
			static void randomize (char *buffer, int length);
	};
};

#endif /* SSH_RANDOM_H */
