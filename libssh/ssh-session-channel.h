/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */


#ifndef __SSH_SESSION_CHANNEL_H__
#define __SSH_SESSION_CHANNEL_H__

#include "ssh-buffer.h"
#include "ssh-channel.h"
#include "ssh-transport.h"

namespace SSH {
	class SessionChannel : public Channel {
		public:
			SessionChannel 		(Transport*	t);
			void ptyReq 		(const string&	term,
					 	 uint32_t	width,
					 	 uint32_t	height,
					 	 uint32_t	pixel_width,
					 	 uint32_t	pixel_height,
					 	 const string&	terminal_modes);
			void subsystem 		(const string&	command);
			void windowChange	(uint32_t	width,
				   	 	 uint32_t	height,
				   	 	 uint32_t	pixel_width,
				   	 	 uint32_t	pixel_height);
	};
};
 


#endif /* __SSH_SESSION_CHANNEL_H__ */
