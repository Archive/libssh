/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */


#ifndef __SSH_CHANNEL_H__
#define __SSH_CHANNEL_H__

#include <string>
#include <vector>

#include "ssh-buffer.h"
#include "ssh-transport.h"

using namespace std;

namespace SSH {

	class Channel;

	class ChannelEOFException {
	};

	class ChannelClosedException {
	};

	class ChannelListener {
		public:
			virtual void channelOpened 	(Channel  *c) {};
			virtual void channelOpenFailure	(Channel  *c) {};
			virtual void channelData	(Channel  *c, 
							 Buffer   *data) {};
			virtual void channelExtendedData(Channel  *c, 
							 uint32_t  type, 
							 Buffer   *data) {};
			virtual void channelEOF		(Channel *c) {};
			virtual void channelClosed	(Channel *c) {};
			virtual void channelRequest	(Channel *c,
							 const string& type,
							 bool want_reply,
							 Buffer	*data) {};


			/* FIXME: uhhh - what happened to these?				     
			Signal4<void,Channel*,uint32_t,string,string> failure;
			Signal4<void,Channel*,string,bool,Buffer*> request_received;
			*/
	};

	class Channel : public PacketHandler {
		public:
				Channel	(Transport*	 t);
			virtual ~Channel ();

			void	write	(const Buffer& b);

			void	write	(const string& s);

			void 	request	(const string& 	type, 
					 bool 	 	want_reply, 
					 const Buffer& 	extra);

			void 	request	(const string& 	type, 
					 bool 	 	want_reply);

			void 	request	(const string&	type, 
					 bool 	 	 want_reply,
					 const string& 	string);

			void	eof	();

			void	close	();

			bool 	handlePacket (Transport *t, const Buffer& p);

			void	addChannelListener (ChannelListener *cl);
			void	removeChannelListener (ChannelListener *cl);

		protected:
			void	open 	(const string& 	type, 
					 uint32_t 	max_window, 
					 uint32_t 	max_packet);

			Transport* 	transport;

		private:
			/* handlers for packet types */
			void 	DATA (Buffer *p);
			void 	EXTENDED_DATA (Buffer *p);
			void	OPEN_CONFIRMATION (Buffer *p);
			void	OPEN_FAILURE (Buffer *p);
			void	WINDOW_ADJUST (Buffer *p);
			void	CHANNEL_EOF (Buffer *p);
			void	CLOSE (Buffer *p);
			void	REQUEST (Buffer *p);

			string	 	channel_type;
			uint32_t	our_channel;
			uint32_t	their_channel;
			uint32_t	recv_window_max;
			uint32_t	send_window;
			uint32_t	recv_window;
			uint32_t	send_packet_max;
			uint32_t	recv_packet_max;

			Buffer* 	pending;

			void 	actuallyWrite (Buffer *b);
			void 	tryToWritePending ();
			void	tryToSendEOFClose ();
			void	windowAdjust (uint32_t size);
			void	sendPacket (char type, const Buffer& b);
			void	sendPacket (char type);

			typedef vector<ChannelListener *> ChannelListeners;
			ChannelListeners		  channelListeners;

			bool		eof_pending;
			bool		eof_sent;
			bool		eof_received;
			bool		close_pending;
			bool		close_sent;
			bool		close_received;
	};
};

#endif /* __SSH_CHANNEL_H__ */
