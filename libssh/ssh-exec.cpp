/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include "ssh-exec.h"
#include "ssh-session-channel.h"
#include "ssh-transport.h"
#include "ssh-channel.h"

#include <string>

using namespace std;

namespace SSH {

Exec::Exec (Transport *t, const string& cmd) : SessionChannel (t) {
	command = cmd; /* FIXME: does this do the right thing? */
	addChannelListener (this);
}

void 
Exec::channelOpened (Channel  *c) {
	Buffer extra;

	extra.appendString (command);

	request ("shell", false, extra);
}

};
