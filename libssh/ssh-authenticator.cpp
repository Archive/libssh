/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include "ssh-authenticator.h"

#include <string>

using namespace std;

namespace SSH {

string *
Authenticator::getUsername () {
	/* default implementation - fail */
	return NULL;
}

string *
Authenticator::getPassword (const string& username) 
{
	/* default implementation - fail */
	return NULL;
}


void 
Authenticator::showBanner (const string& message,
			    const string& language)
{
	/* default implementation - do nothing */
}

void 
Authenticator::success (bool successful)
{
	/* default implementation - do nothing */
}


};
