/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include "ssh-filexfer.h"
#include "ssh-session-channel.h"
#include "ssh-transport.h"
#include "ssh-channel.h"
#include "ssh-protocol.h"

namespace SSH {

/* SSH::Filexfer::Attribute */
Filexfer::Attribute::Attribute (Buffer *b)
{
	uint32_t i, extended_count = 0;
	Buffer *extended_type;
	Buffer *extended_data;

	flags = b->popUint32 ();

	if (flags & SSH_FILEXFER_ATTR_SIZE) { 
		size = b->popUint64 (); 
	}

	if (flags & SSH_FILEXFER_ATTR_UIDGID) {  
		uid = b->popUint32 (); 
		gid = b->popUint32 (); 
	}

	if (flags & SSH_FILEXFER_ATTR_PERMISSIONS) { 
		permissions = b->popUint32 (); 
	}

	if (flags & SSH_FILEXFER_ATTR_ACMODTIME) { 
		atime = b->popUint32 (); 
		mtime = b->popUint32 (); 
	}

	if (flags & SSH_FILEXFER_ATTR_EXTENDED) { 
		extended_count = b->popUint32 (); 
	}

	for (i=0; i<extended_count; i++) {
		extended_type = b->popStringAsBuffer ();
		extended_data = b->popStringAsBuffer ();

		string etype(extended_type->bytes(), extended_type->length());
		string edata(extended_data->bytes(), extended_data->length());

		delete extended_type;
		delete extended_data;

		extended[etype] = edata;
	}
}

void
Filexfer::Attribute::append (Buffer *b)
{
	b->append (flags);

	if (flags & SSH_FILEXFER_ATTR_SIZE) { 
		b->append (size);
	}

	if (flags & SSH_FILEXFER_ATTR_UIDGID) {  
		b->append (uid);
		b->append (gid);
	}

	if (flags & SSH_FILEXFER_ATTR_PERMISSIONS) { 
		b->append (permissions);
	}

	if (flags & SSH_FILEXFER_ATTR_ACMODTIME) { 
		b->append (atime);
		b->append (mtime);
	}

	if (flags & SSH_FILEXFER_ATTR_EXTENDED) { 
		Extended::iterator iter;

		b->append ((uint32_t)extended.size());

		iter = extended.begin ();

		for (iter = extended.begin ();
		     iter != extended.end ();
		     iter++) {
			b->appendString ((*iter).first);
			b->appendString ((*iter).second);
		}
	}

}

/* SSH::Filexfer::Info */
Filexfer::Info::Info (Buffer *b) 
{
	filename = b->popStringAsString ();
	longname = b->popStringAsString ();
	attrs = new Attribute (b);
}
Filexfer::Info::~Info () {
	delete attrs;
}

/* SSH::Filexfer::Response */

Filexfer::Response::Response (uint8_t t, Buffer *b) {
	uint32_t count;

	type = t;
	id = b->popUint32 ();

	switch (t) {
		case SSH_FXP_STATUS:
			/*u.*/status = b->popUint32 ();
			break;
		case SSH_FXP_HANDLE:
			/*u.*/handle = b->popStringAsBuffer ();
			break;
		case SSH_FXP_DATA:
			/*u.*/data = b->popStringAsBuffer ();
			break;
		case SSH_FXP_NAME:
			count = b->popUint32 ();
			for (uint32_t i=0; i<count; i++) {
				/*u.*/name.push_back (new Info (b));
			}
			break;
		case SSH_FXP_ATTRS:
			/*u.*/attrs = new Attribute (b);
			break;
		case SSH_FXP_EXTENDED:
		case SSH_FXP_EXTENDED_REPLY:
			printf ("FXP: WARNING: we don't support extended stuff\n");
			break;
	}
}

Filexfer::Response::~Response () {
	switch (type) {
		case SSH_FXP_HANDLE:
			delete /*u.*/handle;
			break;
		case SSH_FXP_DATA:
			delete /*u.*/data;
			break;
		case SSH_FXP_NAME:
			/* FIXME make an iterator and free this shit...
			uint32_t count = b->popUint32 ();
			for (uint32_t i=0; i<count; i++) {
				u.name.push_back (new Info (b));
			}*/
			break;
		case SSH_FXP_ATTRS:
			delete /*u.*/attrs;
			break;
	}
}

/* SSH::Filexfer */

Filexfer::Filexfer (Transport *t) : SessionChannel (t)
{
	have_header = false;
	next_id = 0;
	have_remote_version = false;
	remote_version = 0;
	addChannelListener (this);
}

void 
Filexfer::channelOpened (Channel  *c) 
{
	subsystem (SUBSYSTEM_FILEXFER);
	ptyReq ("vt100", 80, 24, 640, 480, ""); /* FIXME: why? */
	sendINIT (); /* FIXME: check for subsystem success first? */
}

void
Filexfer::sendPacket (uint8_t type, const Buffer& data)
{
	Buffer buf(data.length()+8); /* plenty-o-room */

	printf ("FXP: type=%d length=%d\n", type, data.length());

	buf.append ((uint32_t)(data.length()+1));	/* length */
	buf.append ((char)type);			/* type */
	buf.append (data);

	Channel::write (buf);
}

uint32_t
Filexfer::sendRequest (uint8_t type, const Buffer& data)
{
	Buffer b(data.length()+sizeof(uint32_t)*2);
	uint32_t id = next_id++; /* FIXME: handle overflow */

	b.append (id);
	b.append (data);
	sendPacket (type, data);

	return id;
}

void
Filexfer::sendINIT ()
{
	Buffer buf;
	buf.append ((uint32_t)SSH_FXP_PROTOCOL_VERSION);
	sendPacket (SSH_FXP_INIT, buf);
}

void
Filexfer::channelData (Channel *c, Buffer *buf)
{
	buffer.append(buf);

	if (have_header) {
		/* length and type have already been read */
		if (buffer.available() >= length-1) {
			Buffer *b = buffer.popBytesAsBuffer (length-1);
			handle (type, b);
			delete b;
			have_header = false;
		} else {
			/* not enough bytes available yet to read 
			 * the whole packet */
			return;
		}
	}

	while (buffer.available () >= 5) { /* enough for the header? */
		length = buffer.popUint32 ();
		printf ("FXP: length = %d\n", length);
		type = buffer.popByte ();
		printf ("FXP: type = %d\n", type);

		if (buffer.available () >= length-1) {
			Buffer *b = buffer.popBytesAsBuffer (length-1);
			handle (type, b);
			delete b;
		} else {
			have_header = true;
			break;
		}
	}
}

void
Filexfer::handle (uint8_t t, Buffer *b) {
	switch (t) {
		case SSH_FXP_INIT:
			printf ("A client should never recieve a SSH_FXP_INIT message\n");
			break;
		case SSH_FXP_VERSION:

			remote_version = b->popUint32 ();
			have_remote_version = true;
			printf ("Server support filexfer version %u\n", remote_version);
			break;
		case SSH_FXP_STATUS:
		case SSH_FXP_HANDLE:
		case SSH_FXP_DATA:
		case SSH_FXP_NAME:
		case SSH_FXP_ATTRS:
		case SSH_FXP_EXTENDED:
		case SSH_FXP_EXTENDED_REPLY:
			handleResponse (t, b);
			break;

		default:
			printf ("unhandled FXP packet type=%u\n", t);
			break;
	}
}

void
Filexfer::handleResponse (uint8_t t, Buffer *b) 
{
	Response *response = new Response (t, b);

	pending_responses.push_back (response);
}

Filexfer::Response* 
Filexfer::getResponse (uint32_t id)
{
	Responses::iterator iter;
	Response *response;

	iter = pending_responses.begin ();

	for (iter = pending_responses.begin ();
	     iter != pending_responses.end ();
	     iter++) {
		response = *iter;

		if (response->id == id) {
			pending_responses.erase (iter);

			return response;
		}
	}
	return NULL;
}

Filexfer::Response*
Filexfer::waitForResponse (uint32_t id) {
	Response *response;

	do {
		response = getResponse (id);
		transport->handlePacket ();
	} while (response == NULL);

	return response;
}


bool
Filexfer::connected () 
{
	return have_remote_version;
}

uint32_t 
Filexfer::open	(const string& filename,
		 uint32_t pflags,
		 Filexfer::Attribute& attrs)
{
	Buffer b;
	b.appendString (filename);
	b.append (pflags);
	attrs.append (&b);
	return sendRequest (SSH_FXP_OPEN, b);
}

uint32_t 
Filexfer::close	(const Buffer& handle)
{
	Buffer b;
	b.appendString (handle);
	return sendRequest (SSH_FXP_CLOSE, b);
}

uint32_t 
Filexfer::read	(const Buffer& handle, 
		 uint64_t offset,
		 uint32_t length)
{
	Buffer b;
	b.appendString (handle);
	b.append (offset);
	b.append (length);
	return sendRequest (SSH_FXP_READ, b);
}

uint32_t 
Filexfer::write	(const Buffer& handle, 
		 uint64_t offset,
		 const Buffer& data)
{
	Buffer b;
	b.appendString (handle);
	b.append (offset);
	b.appendString (data);
	return sendRequest (SSH_FXP_WRITE, b);
}

uint32_t 
Filexfer::remove(const string& filename)
{
	Buffer b;
	b.appendString (filename);
	return sendRequest (SSH_FXP_WRITE, b);
}

uint32_t 
Filexfer::rename(const string& oldpath, 
		 const string& newpath)
{
	Buffer b;
	b.appendString (oldpath);
	b.appendString (newpath);
	return sendRequest (SSH_FXP_RENAME, b);
}

uint32_t 
Filexfer::mkdir	(const string& path)
{
	Buffer b;
	b.appendString (path);
	return sendRequest (SSH_FXP_MKDIR, b);
}

uint32_t 
Filexfer::rmdir	(const string& path)
{
	Buffer b;
	b.appendString (path);
	return sendRequest (SSH_FXP_RMDIR, b);
}

uint32_t 
Filexfer::opendir(const string& path)
{
	Buffer b;
	b.appendString (path);
	return sendRequest (SSH_FXP_OPENDIR, b);
}

uint32_t
Filexfer::readdir (const Buffer& handle) {
	Buffer b;
	b.appendString (handle);
	return sendRequest (SSH_FXP_READDIR, b);
}

uint32_t 
Filexfer::stat	(const string& path)
{
	Buffer b;
	b.appendString (path);
	return sendRequest (SSH_FXP_STAT, b);
}

uint32_t 
Filexfer::lstat	(const string& path)
{
	Buffer b;
	b.appendString (path);
	return sendRequest (SSH_FXP_LSTAT, b);
}

uint32_t 
Filexfer::fstat	(const Buffer& handle)
{
	Buffer b;
	b.appendString (handle);
	return sendRequest (SSH_FXP_FSTAT, b);
}

uint32_t 
Filexfer::setstat(const string& path, 
		  Filexfer::Attribute& attr)
{
	Buffer b;
	b.appendString (path);
	attr.append (&b);
	return sendRequest (SSH_FXP_SETSTAT, b);
}

uint32_t 
Filexfer::fsetstat(const Buffer& handle, 
		   Filexfer::Attribute& attr)
{
	Buffer b;
	b.appendString (handle);
	attr.append (&b);
	return sendRequest (SSH_FXP_FSETSTAT, b);
}

uint32_t 
Filexfer::realpath(const string& path)
{
	Buffer b;
	b.appendString (path);
	return sendRequest (SSH_FXP_REALPATH, b);
}

};
