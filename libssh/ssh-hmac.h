/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#ifndef SSH_HMAC_H
#define SSH_HMAC_H

#include "ssh-buffer.h"

#include <openssl/evp.h>

#include <string>

using namespace std;

namespace SSH {
	class HMac {
		public:
			/* class methods */
			static string availableAlgorithms ();
			static HMac* forAlgorithm (const string& algorithm, const Buffer& k);
			static string chooseAlgorithm (const string& client_algorithms,
					const string& server_algorithms);
			static HMac* none ();

			/* instance methods */
			virtual uint32_t length () const = 0;
			virtual Buffer* operator()(uint32_t sequence, const Buffer& packet) = 0;
	};
};

#endif /* SSH_HMAC_H */
