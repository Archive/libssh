/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

/* this code covers the protocols described in:
 * draft-ietf-secsh-userauth-11.txt
 * ie: it covers the process of going through various authentication methods.
 */

#ifndef __SSH_USERAUTH_H__
#define __SSH_USERAUTH_H__

#include "ssh-authenticator.h"
#include "ssh-packethandler.h"
#include "ssh-transport.h"

#include <string>

using namespace std;

namespace SSH {
	class Userauth : public PacketHandler {
		public:
				Userauth 	(Transport *t, 
					 	 Authenticator *a);
			virtual ~Userauth	();

			bool	handlePacket	(Transport *t, 
						 const Buffer& b);
		protected:
			Transport	*transport;
			int 		state;
			Authenticator	*authenticator;

			static const int 	STATE_INITIAL = 0;
			static const int	STATE_PASSWORD = 1;
			static const int 	STATE_FAILED = 2;

		private:
			void authPassword ();
			void nextStep ();
			bool recvSERVICE_ACCEPT (Buffer *p);
	};

/* FIXME: support public key crypto (required by spec) */
/* FIXME: support PASSWD_CHANGEREQ */
/* FIXME: support host based authentication */
};

#endif /* __SSH_USERAUTH_H__ */
