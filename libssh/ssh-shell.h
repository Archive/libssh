/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */


#ifndef __SSH_SHELL_H__
#define __SSH_SHELL_H__

#include "ssh-channel.h"
#include "ssh-session-channel.h"
#include "ssh-buffer.h"
#include "ssh-transport.h"

namespace SSH {
	class Shell :	public SessionChannel, 
			protected ChannelListener {
		public:
			Shell 				(Transport*	t);

		protected:
			virtual void channelOpened 	(Channel  *c);
	};
};
 


#endif /* __SSH_SHELL_H__ */

