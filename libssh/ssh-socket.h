/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include "ssh-stream.h"

#include <string>
#include <unistd.h>
#include <errno.h>

#include <string>

using namespace std;

#ifndef SSH_SOCKET_H
#define SSH_SOCKET_H

namespace SSH {

	class SocketException {
		public:
			SocketException (const string& ctx) {
				context = ctx;
				exception_errno = errno;
			};

			string context;
			int exception_errno;
	};

	/*
	 * This is an implementation of socket.
	 */
	class Socket : public Stream {
		public:
			Socket (const std::string& host, int port=22);
			virtual ~Socket () { ::close (filedes); }

			virtual void read (char	*buffer,
					   int	 length,
					   int	*length_read);
			virtual void write (const char	*buffer,
					    int		 length,
					    int		*length_written);

			virtual int fd () { return filedes; }

		private:
			int filedes;
	};

} /* namespace SSH */


#endif /* SSH_SOCKET_H */


