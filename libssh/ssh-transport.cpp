/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <openssl/dh.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>

#include <assert.h>

#include "ssh-private.h"
#include "ssh-transport.h"
#include "ssh-buffer.h"
#include "ssh-protocol.h"
#include "ssh-engine.h"
#include "ssh-blowfish-engine.h"
#include "ssh-bignum.h"
#include "ssh-hmac.h"
#include "ssh-userauth.h"

#define PACKET_MAX_LENGTH 35000

#define BUFFER_SIZE 4096

namespace SSH {

Channel *
Transport::channel (uint32_t id) {
	return channels[id];
}

Channel *
Transport::channel (const Buffer& b)
{
	Buffer p(b);

	return channel(p.popUint32 ());
}

uint32_t
Transport::addChannel (Channel *c)
{
	uint32_t i;

	/* LOCK ME */
	i = next_channel_id;
	channels[i] = c;
	next_channel_id++;
	/* UNLOCK ME */

	return i;
}

void
Transport::bufferFill ()
{
	char temp_buffer[BUFFER_SIZE];
	char *new_buffer;
	int len = 0;

	//printf ("bufferFill() starts ib=%p ib_len=%d\n", internal_buffer, internal_buffer_len);

	stream->read (&temp_buffer[0], BUFFER_SIZE-1, &len);

	if (len == 0) {
		return; /* don't need to shuffle buffers */
	}

	new_buffer = new char[internal_buffer_len+len];
	memcpy (new_buffer, internal_buffer, internal_buffer_len);
	memcpy (new_buffer+internal_buffer_len, temp_buffer, len);
	delete[] internal_buffer_base_ptr;
	internal_buffer = new_buffer;
	internal_buffer_base_ptr = new_buffer;
	internal_buffer_len = internal_buffer_len+len;
}

void
Transport::bufferFill (uint32_t length)
{
	//printf ("bufferFill(%d)\n", length);
	while (internal_buffer_len < length) {
		bufferFill ();
	}
}

uint32_t
Transport::readUint32 ()
{
	uint32_t data;

	bufferFill (4);

	data = ntohl(*((uint32_t*)internal_buffer));

	internal_buffer_len -= 4;
	internal_buffer += 4;

	return data;
}

Buffer*
Transport::readBytes (uint32_t length)
{
	Buffer* bytes;

	bufferFill (length);

	bytes = new Buffer ((const char*)internal_buffer, length);

	internal_buffer_len -= length;
	internal_buffer += length;

	return bytes;
}

char
Transport::readByte ()
{
	bufferFill (1);

	internal_buffer_len--;
	internal_buffer++;

	return internal_buffer[-1];
}

char
Transport::peekByte ()
{
	bufferFill (1);

	return internal_buffer[0];
}

#define MAX_LINE_LEN 4096
char *
Transport::readLine ()
{
	char line[MAX_LINE_LEN];
	int i;
	char c;

	for (i=0; i<MAX_LINE_LEN-1; i++) {
		c = readByte ();
		if (c == '\r' && peekByte () == '\n') {
			readByte ();
			break;
		}
		if ( (c == '\n') || (c == '\0')) {
			break;
		}
		line[i] = c;
	}
	line[i] = '\0';
	return strdup (line);
}

void
Transport::write (const char *bytes, uint32_t len)
{
	int 		 bytes_written;
	stream->write (bytes, len, &bytes_written);
}

void
Transport::write (Buffer *p)
{
	write (p->bytes (), p->length ());
}

void
Transport::write (const Buffer& p)
{
	write (p.bytes (), p.length ());
}

void
Transport::write (const std::string& string)
{
	write (string.c_str (), string.length ());
}

Buffer *
Transport::readBlock ()
{
	Buffer *b;

	bufferFill (block_size);

	b = new Buffer ((const char *)internal_buffer, block_size);

	internal_buffer_len -= block_size;
	internal_buffer += block_size;

	return b;
}

Buffer *
Transport::recvPacket ()
{
	Buffer *p;
	unsigned char padding_length;
	uint32_t packet_length;
	int payload_length;
	Buffer *mac_read, *mac_calculated;
	Buffer *block_ciphertext, *block_plaintext;
	int num_blocks;
	int bytes_read;

	if (engine_in == NULL) { /* no crypto yet */
		packet_length = readUint32 ();
		padding_length = readByte ();
		payload_length = packet_length - padding_length - 1;
		p = readBytes (payload_length), payload_length;

		/* skip padding */
		delete readBytes (padding_length);
	} else {
		bytes_read = 0;
		/* read the first block */
		block_ciphertext = readBlock ();
		bytes_read += block_size;
		block_plaintext = engine_in->decrypt (*block_ciphertext);
		packet_length = block_plaintext->popUint32 ();
		padding_length = block_plaintext->popByte ();
		payload_length = packet_length - padding_length - 1;
		/* so, packet_length should be a multiple of ssh_buffer_size */
		num_blocks = (packet_length + sizeof (packet_length)) / 
					block_size;
		num_blocks--; /* we've already read one */
		p = new Buffer ();
		p->append (block_plaintext->bytes () +
				(sizeof(packet_length)+sizeof(padding_length)),
				block_plaintext->length () -
				(sizeof(packet_length)+sizeof(padding_length)));
		delete block_plaintext;
		delete block_ciphertext;
		while (num_blocks > 0) {
			block_ciphertext = readBlock ();
			bytes_read += block_size;
			block_plaintext = engine_in->decrypt (*block_ciphertext);
			p->append (block_plaintext);
			delete block_plaintext;
			delete block_ciphertext;
			num_blocks--;
		}
	}

	/* we need to reconstruct the original packet (prepend the packet 
	 * length and padding length to the plaintext, basically) */
	Buffer mac_in_input;
	mac_in_input.append (packet_length);
	mac_in_input.append ((char)padding_length);
	mac_in_input.append (p);
	
	mac_read = readBytes (mac_in->length());
	mac_calculated = (*mac_in)(sequence_number_in, mac_in_input);

	if (!mac_read->equals(mac_calculated)) {
		mac_read->debug ("read");
		mac_calculated->debug ("calculated");
		throw new TransportException ("MACs don't match");
	}
	delete mac_read;
	delete mac_calculated;

	p->truncate (payload_length); /* drop the padding */

	sequence_number_in++;
	sequence_number_in %= 0x10000000;

	return p;
}

void
Transport::sendPacket (const Buffer& p) 
{
	unsigned char padding_length=0;
	int packet_length;
	Buffer *mac;

	printf ("[YAK] ssh_transport_send_packet type=%d (%d bytes)\n", 
			(int)*(p.bytes()), p.length ());

	p.debug ("sendPacket");

	padding_length = block_size - ((p.length () + 1) % 
				block_size) + 4;

	Buffer *padding = Buffer::random (padding_length);

	packet_length = p.length () + 1 + padding_length;

	/* build packet */
	Buffer plaintext;
	plaintext.append ((uint32_t) packet_length);
	plaintext.append ((const char *)&padding_length, 1);
	plaintext.append (p.bytes (), p.length ());
	plaintext.append (padding);

	delete padding;

	/* calculate MAC */
	mac = (*mac_out)(sequence_number_out, plaintext);

	Buffer *ciphertext;
	/* encrypt the packet */
	if (engine_out != NULL) {
		/*ciphertext = engine_out (t, plaintext);*/
		ciphertext = engine_out->encrypt (plaintext);
	} else {
		ciphertext = new Buffer (plaintext);
	}

	sequence_number_out++;
	sequence_number_out %= 0x10000000;

	plaintext.debug (">plaintext");
	ciphertext->debug (">ciphertext");
	mac->debug (">mac");

	write (ciphertext);
	write (mac);

	delete ciphertext;
	delete mac;
}

void
Transport::sendKEXINIT ()
{
	Buffer *cookie = Buffer::random (16);

	Buffer p;

	printf ("sending KEXINIT\n");
	cookie->debug ("cookie");

	p.append ((char)SSH_MSG_KEXINIT);
	p.append (cookie);

	delete cookie;

	/* send kex_algorithms */
	p.appendString ("diffie-hellman-group1-sha1");

	/* server_host_key_algorithms */
        p.appendString ("ssh-rsa");

	/* encryption_algorithms_client_to_server */
        p.appendString ("blowfish-cbc");

	/* encryption_algorithms_server_to_client */
        p.appendString ("blowfish-cbc");

	/* mac_algorithms_client_to_server */
        p.appendString (HMac::availableAlgorithms ());

	/* mac_algorithms_server_to_client */
        p.appendString (HMac::availableAlgorithms ());

	/* compression_algorithms_client_to_server */
        p.appendString ("none");

	/* compression_algorithms_server_to_client */
        p.appendString ("none");

	/* languages_client_to_server */
        p.appendString ("");

	/* languages_server_to_client */
        p.appendString ("");

	/* first_kex_packet_follows */
	p.append (false);

	/* reserved for future extension */
	p.append ((uint32_t)0);

	p.debug ("KEXINIT");

	/* send the packet */
	sendPacket (p);

	I_C = new Buffer (p);
}

void
Transport::sendKEXDH_INIT ()
{
	dh = new DiffieHellman ();

	e = dh->publicKey ();

	e->debug ("Cli Pub Key");

	Buffer p;
	p.append ((char)SSH_MSG_KEXDH_INIT);
	p.append (e);
	sendPacket (p);

	/* FIXME: save p? */
}

void
Transport::sendNEWKEYS ()
{
	Buffer p;
	printf ("sending NEWKEYS\n");
	p.append ((char) SSH_MSG_NEWKEYS);
	sendPacket (p);
}

void
Transport::recvKEXDH_REPLY (const Buffer& b)
{
	Buffer *K_S;
	Buffer *sig_H;
	Buffer p(b);

	b.debug ("<KEXDH_REPLY");

	p.popByte (); /* skip type */

	K_S = p.popStringAsBuffer ();

	f = p.popBignum ();
	f->debug ("f");
	
	sig_H = p.popStringAsBuffer ();

	K = dh->computeKey (f);

	/* calulate:
	 * H = hash (V_C || V_S || I_C || I_S || K_S || e || f || K)
	 */

	Buffer H_in;
	H_in.appendString (V_C);
	H_in.appendString (V_S);
	H_in.appendString (I_C);
	H_in.appendString (I_S);
	H_in.appendString (K_S);
	H_in.append (e);
	H_in.append (f);
	H_in.append (K);
	H = H_in.hash ();

	H->debug ("H");

	/* FIXME: check server host key signature thingo */

	/* .... */
	/* FIXME: free this shit */

	sendNEWKEYS ();

	printf ("finished handling KEXDH_REPLY\n");
}

Buffer *
Transport::computeKey (char id, uint32_t length)
{
	Buffer *hash;

	Buffer b;
	/* build input to hash */
	b.append (K);
	b.append (H);
	b.append (id);
	b.append (H);

	/* hash it */
	hash = b.hash();

	/* truncate hash to desired length */
	assert (hash->length() >= length); /* make sure theres enough */
	hash->truncate (length);

	char x[400];
	sprintf (x, "key '%c'", id);
	hash->debug (x);

	return hash;
}

void
Transport::recvNEWKEYS (const Buffer& p)
{
	Buffer *key_out;
	Buffer *key_in;
	Buffer *mac_key_out;
	Buffer *mac_key_in;

	printf ("recieved NEWKEYS\n");

	/* work out hashes */
	IV_out = computeKey ('A', 8);

	IV_in = computeKey ('B', 8);

	key_out = computeKey ('C', 16);

	key_in = computeKey ('D', 16);

	mac_key_out = computeKey ('E', 20);

	mac_key_in = computeKey ('F', 20);

	/* FIXME: support other crypto */

	/* setup blowfish engines */
	engine_out = new BlowfishEngine (*key_out, *IV_out);
	engine_in = new BlowfishEngine (*key_in, *IV_in);

	/* setup MAC engines */
	delete mac_out;
	mac_out = HMac::forAlgorithm (mac_algorithm_client_to_server,
			*mac_key_out);
	delete mac_key_out;
	delete mac_in;
	mac_in = HMac::forAlgorithm (mac_algorithm_server_to_client,
			*mac_key_in);
	delete mac_key_in;

	connected ();
}


void
Transport::sendSERVICE_REQUEST (const string& service)
{
	Buffer p;

	printf ("sending SERVICE_REQUEST(%s)\n", service.c_str());

	p.append ((char) SSH_MSG_SERVICE_REQUEST);
	p.appendString (service);
	sendPacket (p);
}

void 
Transport::recvKEXINIT (const Buffer& p)
{
	/* read the server's KEXINIT */
	I_S = new Buffer (p);

	I_S->seekRelative (1); /* skip type */
	I_S->seekRelative (16); /* skip cookie */

	string kex_algorithms = I_S->popStringAsString ();
	string server_host_key_algorithms = I_S->popStringAsString ();
	string encryption_algorithms_client_to_server = I_S->popStringAsString ();
	string encryption_algorithms_server_to_client = I_S->popStringAsString ();
	string mac_algorithms_client_to_server = I_S->popStringAsString ();
	string mac_algorithms_server_to_client = I_S->popStringAsString ();
	mac_algorithm_client_to_server = 
		HMac::chooseAlgorithm (HMac::availableAlgorithms(), 
				mac_algorithms_client_to_server);
	mac_algorithm_server_to_client = 
		HMac::chooseAlgorithm (HMac::availableAlgorithms(), 
				mac_algorithms_server_to_client);
	string compression_algorithms_client_to_server = I_S->popStringAsString ();
	string compression_algorithms_server_to_client = I_S->popStringAsString ();
	string languages_client_to_server = I_S->popStringAsString ();
	string languages_server_to_client = I_S->popStringAsString ();

//	mac_algorithms_client_to_server = HMac::availableAlgorithms (); //SUCK
//	mac_algorithms_server_to_client = HMac::availableAlgorithms (); //SUCK

	/* lets do diffie-hellman-group1-sha1 */
	sendKEXDH_INIT ();
}

void 
Transport::recvSERVICE_ACCEPT (const Buffer& b)
{
	char *service_name;
	Buffer p(b);

	service_name = p.popStringAsCstring ();
}

void
Transport::addPacketHandler (PacketHandler *ph)
{
	packetHandlers.push_back (ph);
}

void
Transport::removePacketHandler (PacketHandler *ph)
{
	PacketHandlers::iterator	iter;

	for (iter = packetHandlers.begin();
			iter != packetHandlers.end();
			iter++) {
		if (*iter == ph) {
			packetHandlers.erase (iter);
			return;
		}
	}
}

void
Transport::handlePacket ()
{
	unsigned char			type;
        Buffer*				p;
	bool		 		handled;
	PacketHandlers::iterator	iter;
	
	p = recvPacket ();

	p->debug ("handlePacket");

	for (iter = packetHandlers.begin();
			iter != packetHandlers.end();
			iter++) {
		handled = (*iter)->handlePacket (this, *p);
		if (handled) {
			return;
		}
	}

	p->seek (0);
	type = p->popByte ();
	printf ("handlePacket: recieved packet type %d\n", type);
	switch (type) {
		case SSH_MSG_UNIMPLEMENTED:
#ifdef DEBUG
			printf ("UNIMPLEMENTED for packet %d\n", 
					p->popUint32 ());
#endif
			break;

		case SSH_MSG_IGNORE:
			printf ("[YAK] ignoring packet\n");
			p->debug ("ignoring");
			break;

		case SSH_MSG_KEXINIT:
			recvKEXINIT (*p);
			break;

		case SSH_MSG_KEXDH_REPLY:
			recvKEXDH_REPLY (*p);
			break;

		case SSH_MSG_NEWKEYS:
			recvNEWKEYS (*p);
			break;

		case SSH_MSG_SERVICE_ACCEPT:
			recvSERVICE_ACCEPT (*p);
			break;

		default:
#ifdef DEBUG
			printf ("[YAK] unknown packet type %d\n", type);
#endif
			break;
	}
}

Transport::Transport (Stream *s,
		      Authenticator *a)
{
	block_size = 8;
	stream = s;
	authenticator = a;

	/* set up a dummy initial buffer */
	internal_buffer = new char[1];
	internal_buffer_base_ptr = internal_buffer;
	internal_buffer_len = 0;

	/* set up MAC for both directions with "none" algorithm */
	mac_in = HMac::none ();
	mac_out = HMac::none ();

	/* send our version info */
	V_C = strdup ("SSH-2.0-libssh_0.0");
	write (V_C);
	write ("\r\n");

	/* read their version info */
	for (;;) {
		V_S = readLine ();
		if (!strncmp ("SSH-", V_S, 4)) {
			break;
		}
	}

	/* send our KEXINIT */
	sendKEXINIT ();
}

void
Transport::addTransportListener (TransportListener *ph)
{
	transportListeners.push_back (ph);
}

void
Transport::removeTransportListener (TransportListener *ph)
{
	TransportListeners::iterator	iter;

	for (iter = transportListeners.begin();
			iter != transportListeners.end();
			iter++) {
		if (*iter == ph) {
			transportListeners.erase (iter);
			return;
		}
	}
}

void
Transport::connected ()
{
	TransportListeners::iterator	iter;

	for (iter = transportListeners.begin(); 
			iter != transportListeners.end(); 
			iter++) {
		(*iter)->transportConnected (this);
	}

	if (authenticator != NULL) {
		userauth = new Userauth (this, authenticator);
	}
}

void
Transport::authenticated ()
{
	TransportListeners::iterator	iter;

	for (iter = transportListeners.begin(); 
			iter != transportListeners.end(); 
			iter++) {
		(*iter)->transportAuthenticated (this);
	}
}

}; /* namespace SSH */
