/* a new set of buffer classes */

#ifndef SSH_BUFFER2_H
#define SSH_BUFFER2_H

#include <stdint.h>
#include <string>
#include <vector>
#include <stdio.h> // for printf in Buffer2::debug
#include <netdb.h> // for htonl

#include "ssh-refcounted.h"

using namespace std;

namespace SSH {

	/* the common interface for readable and writable buffers */
	class Buffer2 : public Refcounted {
		public:
			/** the number of bytes this buffer occupies */
			virtual uint32_t getLength() const = 0;

			/** copy the data from the buffer to address b */
			virtual void getBytes(char *b) const = 0;

			virtual void debug (const string& prefix) const;
	};

	class ListBuffer : public Buffer2 {
		public:
			virtual uint32_t getLength() const;
			virtual void getBytes(char *b) const;
			virtual ~ListBuffer ();

			void append (Buffer2 *buffer) {
				buffer->ref ();
				children.push_back (buffer);
			}
		private:
			vector<Buffer2 *> children;
	};

	class ByteBuffer : public Buffer2 {
		public:
			virtual ~ByteBuffer () {};
			ByteBuffer (unsigned char byte) { data = byte; }
			virtual uint32_t getLength() const { return 1; }
			virtual void getBytes(char *b) const 
					{ *b = (char)data; }
		private:
			unsigned char data;
	};
	
	class BooleanBuffer : public Buffer2 {
		public:
			virtual ~BooleanBuffer () {};
			BooleanBuffer (bool boolean) { data = boolean; }
			virtual uint32_t getLength() const { return 1; }
			virtual void getBytes(char *b) const 
					{ *b = data?'\1':'\0'; }
		private:
			bool data;
	};

	class Uint32Buffer : public Buffer2 {
		public:
			virtual ~Uint32Buffer () {};
			Uint32Buffer (uint32_t uint32) { data = htonl(uint32); }
			virtual uint32_t getLength() const { return 4; }
			virtual void getBytes(char *b) const {
				memcpy (b, (const char*)&data, getLength ());
			}
		private:
			uint32_t data;
	};

	class Uint64Buffer : public Buffer2 {
		public:
			virtual ~Uint64Buffer () {};
			Uint64Buffer (uint64_t i) { 
				data = ((((i) & 0xff00000000000000ull) >> 56) |\
			   		(((i) & 0x00ff000000000000ull) >> 40) |\
			   		(((i) & 0x0000ff0000000000ull) >> 24) |\
			   		(((i) & 0x000000ff00000000ull) >> 8)  |\
			   		(((i) & 0x00000000ff000000ull) << 8)  |\
			   		(((i) & 0x0000000000ff0000ull) << 24) |\
			   		(((i) & 0x000000000000ff00ull) << 40) |\
			   		(((i) & 0x00000000000000ffull) << 56));
			}
			virtual uint32_t getLength() const { return 8; }
			virtual void getBytes(char *b) const {
				memcpy (b, (const char*)&data, getLength ());
			}
		private:
			uint64_t data;
	};

	class BytesBuffer : public Buffer2 {
		public:
			BytesBuffer (const char* bytes, 
				    uint32_t len) {
				data = new char[len];
				memcpy (data, bytes, len);
				length = len;
			}
			virtual ~BytesBuffer () {
				delete[] data;
			}
			virtual uint32_t getLength () const { return length; }
			virtual void getBytes (char *b) const {
				memcpy (b, data, length);
			}
		private:
			char*		data;
			uint32_t	length;
	};

	class StringBuffer : public ListBuffer {
		public:
			StringBuffer (string str) { 
				append (new Uint32Buffer (str.length()));
				append (new BytesBuffer (str.c_str(), 
							 str.length()));
			}
	};

};

#endif /* SSH_BUFFER2_H */
