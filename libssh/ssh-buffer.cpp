/* part of libssh
 * (c) Copyright 2001, 2002 Ian McKellar <yakk@yakk.net>
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <openssl/dh.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <ctype.h>

#include <assert.h>

#include "ssh-buffer.h"
#include "ssh-random.h"
#include "ssh-bignum.h"

#define MAX_PACKET_LENGTH 35000

namespace SSH {

Buffer::~Buffer () 
{
	if (payload != NULL) {
		free (payload);
	}
}

Buffer::Buffer ()
{
	payload = NULL;
	setPayload (MAX_PACKET_LENGTH);
}

Buffer::Buffer (char *bytes, uint32_t length)
{
	payload = NULL;
	setPayload (bytes, length);
}

Buffer::Buffer (const char *bytes, uint32_t length) 
{
	payload = NULL;
	setPayload (bytes, length);
}

Buffer::Buffer (uint32_t length)
{
	payload = NULL;
	setPayload (length);
}

Buffer::Buffer (const Buffer &buffer)
{
	printf ("(calling Buffer copy constructor)\n");
	payload = NULL;
	setPayload ((const char*)buffer.payload, buffer.length ());
}

Buffer::Buffer (const string& str)
{
	payload = NULL;
	setPayload (str.c_str (), str.length ());
}

void
Buffer::setPayload (const char *bytes, uint32_t length)
{
	char *allocated;

	allocated = (char *)malloc (length);
	memcpy (allocated, bytes, length);

	setPayload (allocated, length);
}

void
Buffer::setPayload (char *bytes, uint32_t length)
{
	if (payload != NULL) {
		free (payload);
	}
	payload_length = length;
	payload = bytes;
	buffer_size = length;
	index = 0;
}

void
Buffer::setPayload (uint32_t length)
{
	char *allocated;

	allocated = (char *)malloc (length);
	setPayload (allocated, length);
	payload_length = 0;
}


void
Buffer::append (char byte)
{
	assert (buffer_size - payload_length > 1);

	payload[payload_length] = byte;
	payload_length++;
}

void
Buffer::append (const char *bytes, uint32_t length) 
{
	assert (buffer_size - payload_length > length);

	memcpy (payload+payload_length, bytes, length);
	payload_length += length;
}

void
Buffer::append (uint32_t uint32)
{
	uint32_t data = htonl (uint32);
	append ((char *)&data, sizeof (data));
}


#define HTONLL(x) ((((x) & 0xff00000000000000ull) >> 56) | \
                   (((x) & 0x00ff000000000000ull) >> 40) | \
                   (((x) & 0x0000ff0000000000ull) >> 24) | \
                   (((x) & 0x000000ff00000000ull) >> 8) | \
                   (((x) & 0x00000000ff000000ull) << 8) | \
                   (((x) & 0x0000000000ff0000ull) << 24) | \
                   (((x) & 0x000000000000ff00ull) << 40) | \
                   (((x) & 0x00000000000000ffull) << 56))

void
Buffer::append (uint64_t uint64) {
	uint64_t data = HTONLL(uint64);
	append ((char *)&data, sizeof (data));
}

void
Buffer::append (const Buffer& b)
{
	append (b.payload, b.payload_length);
}

void
Buffer::append (const Buffer* b)
{
	append (b->payload, b->payload_length);
}

void
Buffer::append (bool boolean)
{
	if (boolean) {
		append ((char)1);
	} else {
		append ((char)0);
	}
}

void
Buffer::appendString (const char *string, uint32_t length)
{
	append (length);
	append (string, length);
}

void
Buffer::appendString (const Buffer& b)
{
	appendString (b.payload, b.payload_length);
}

void
Buffer::appendString (Buffer *b)
{
	appendString (b->payload, b->payload_length);
}

void
Buffer::appendString (std::string string)
{
	appendString (string.c_str(), string.length());
}

void
Buffer::append (Bignum *value)
{
	Buffer *b;

	b = value->binary ();

	append (b);

	delete b;
}

unsigned char
Buffer::popByte () 
{
	unsigned char byte;

	assert (payload_length-index >= 1);

	byte = payload[index];
	
	index++;

	return byte;
}

uint32_t
Buffer::popUint32 ()
{
	uint32_t uint32;

	assert (payload_length-index >= sizeof(uint32));

	uint32 = *((uint32_t *)(payload+index));
	uint32 = ntohl(uint32);

	index += sizeof(uint32);

	return uint32;
}

uint64_t
Buffer::popUint64 ()
{
	uint64_t uint64;

	assert (payload_length-index >= sizeof(uint64));

	uint64 = *((uint64_t *)(payload+index));
	uint64 = ntohl(uint64);

	index += sizeof(uint64);

	return uint64;
}

Buffer *
Buffer::popBytesAsBuffer (uint32_t length)
{
	assert (payload_length-index >= length);

	Buffer *b = new Buffer ((const char *)(payload+index), length);
	
	index += length;

	return b;
}

char *
Buffer::popString (uint32_t *length)
{
	char *bytes;

	*length = popUint32 ();

	assert (payload_length-index >= *length);

	bytes = (char *)malloc ((*length)+1);
	memcpy (bytes, payload+index, *length);
	bytes[*length] = '\0'; /* ensure bytes is null terminated */

	index += *length;

	return bytes;
}

Buffer *
Buffer::popStringAsBuffer ()
{
	char *buf;
	uint32_t length;

	buf = popString (&length);

	return new Buffer (buf, length);
}

char *
Buffer::popStringAsCstring ()
{
	uint32_t length; /* we'll discard */
	
	/* popString already nul-terminates the string */
	return popString (&length);
}

string
Buffer::popStringAsString ()
{
	char *buf;
	uint32_t length;

	buf = popString (&length);

	return string (buf, length);
}

Bignum *
Buffer::popBignum ()
{
	char *bytes;
	uint32_t length;
	Bignum *bn;

	bytes = popString (&length);

	bn = new Bignum (bytes, length);

	free (bytes);

	return bn;
}

Buffer *
Buffer::hash () const
{
	unsigned char digest[EVP_MAX_MD_SIZE];
	EVP_MD *evp_md = EVP_sha1();
	EVP_MD_CTX md;

	EVP_DigestInit(&md, evp_md);
	EVP_DigestUpdate(&md, payload, payload_length);
	EVP_DigestFinal(&md, digest, NULL);

	return new Buffer((const char *)digest, evp_md->md_size);
}

void
Buffer::truncate (uint32_t length)
{
	assert (length <= payload_length);

	/* FIXME: actually free some shit? */

	payload_length = length;
}

void
Buffer::seek (uint32_t index)
{
	assert (index < payload_length);
	index = index;
}

void
Buffer::seekRelative (int32_t idx)
{
	assert (index+idx < payload_length);
	index += idx;
}

Buffer *
Buffer::concat (const Buffer &a, const Buffer &b)
{
	Buffer *q;

	q = new Buffer (a.payload_length + b.payload_length);

	memcpy (q->payload, a.payload, a.payload_length);
	memcpy (q->payload+a.payload_length, b.payload, b.payload_length);
	q->payload_length = a.payload_length + b.payload_length;

	return q;
}

Buffer *
Buffer::random (uint32_t length)
{
	Buffer *b = new Buffer(length);

	b->payload_length = length;
	Random::randomize (b->payload, length);

	return b;
}

const char *
Buffer::bytes () const
{
	return payload;
}

char *
Buffer::copyBytes () const
{
	char *buf;

	buf = (char *) malloc (payload_length);

	memcpy (buf, payload, payload_length);

	return buf;
}

uint32_t
Buffer::length () const
{
	return payload_length;
}

uint32_t
Buffer::available () const
{
	return (payload_length - index);
}

/**
 * get a new buffer containing all the bytes not popped off yet
 */
Buffer *
Buffer::remaining () const
{
	return new Buffer ((const char*)(payload+index), available());
}

/**
 * discard already read bytes
 */
#if 0
void
Buffer::compact ()
{
	char *oldpayload = payload;

	setPayload ((const char *)(payload+index), buffer_size-index);

	free (oldpayload);
}
#endif

void
Buffer::debug (const string& prefix) const
{
	uint32_t i, j;
	uint32_t rowlen = 16;
	const char *c_prefix = prefix.c_str();

	for (i=0; i<payload_length; i+=rowlen) {
		printf ("%12.12s ", c_prefix);
		for (j=0; j<rowlen && (i+j)<payload_length; j++) {
			printf ("%02X ", (unsigned char)payload[i+j]);
		}

		for (; j<rowlen; j++) {
			printf ("   ");
		}

		for (j=0; j<rowlen && (i+j)<payload_length; j++) {
			if (isprint (payload[i+j])) {
				printf ("%c", (unsigned char)payload[i+j]);
			} else {
				printf (".");
			}
		}

		printf ("\n");
	}
}

bool
Buffer::equals (Buffer *b) const
{
	if ((b->payload_length != payload_length) 
			|| (b->index != index)
			|| memcmp (b->payload, payload, payload_length)) {
		return false;
	}

	return true;
}

}
