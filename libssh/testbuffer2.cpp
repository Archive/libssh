#include "ssh-buffer2.h"
#include <stdio.h>

using namespace SSH;

int
main (int argc, char** argv) {

	printf ("Testing WritableBuffer\n");

	ListBuffer list;

	list.append (new Uint32Buffer(42));
	list.append (new StringBuffer("hello cruel world"));

	list.debug("list");

}
